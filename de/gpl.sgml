<appendix id="GPL" label="A">
 <title>GNU General Public License</title>
  <para>
   Version 2, June 1991
  </para>
  <para>
   Copyright (C) 1989, 1991 Free Software Foundation, Inc.
  </para>
  <para> 
   675 Mass Ave, Cambridge, MA 02139, USA
  </para>
  <para>
   Jeder hat das Recht, diese Lizenzurkunde zu vervielf&auml;ltigen und unver&auml;nderte Kopien dieses Lizenzdokuments weiterzugeben. Es ist jedoch nicht zul&auml;ssig, &Auml;nderungen an diesem Dokument vorzunehmen.
  </para>
  
   <bridgehead>Vorwort</bridgehead>
    <para>
      Die Lizenzen f&uuml;r die meisten Anwendungsprogramme sollen verhindern, da&szlig; diese weitergegeben und ver&auml;ndert werden. Im Gegensatz dazu soll mit der GNU General Public License sichergestellt werden, da&szlig; 
freie Software von jedem weitergegeben und benutzt werden kann, um dadurch zu gew&auml;hrleisten, da&szlig; die Software wirklich f&uuml;r alle Benutzer frei ist. Diese General Public License gilt f&uuml;r den Gro&szlig;teil 
der von der Free Software Foundation herausgegebenen Software und f&uuml;r alle anderen Programme, deren Autoren ihr Werk dieser Lizenz unterstellt haben. (Ein anderer Teil der Software der Free Software Foundation unterliegt 
statt dessen der GNU Library General Public License). Sie haben die M&ouml;glichkeit, auch Ihre Programme unter diese Lizenz zu stellen.
</para>
    <para>
      Die Bezeichnung &quot;freie Software&quot; bezieht sich auf Freiheit - und nicht auf den Preis. Durch unsere General Public Licenses haben Sie die Freiheit, Kopien freier Software zu verbreiten (und etwas f&uuml;r diesen 
Service zu berechnen, wenn Sie dies m&ouml;chten), den Quellcode mitgeliefert zu bekommen oder auf Wunsch zu erhalten, die Software zu &auml;ndern oder Teile davon in neuen freien Programmen zu verwenden. In unserer Lizenz wird 
Ihnen ausdr&uuml;cklich das Recht einger&auml;umt, dies zu tun.</para>
    <para>
      Zum Schutz Ihrer Rechte m&uuml;ssen wir gewisse Einschr&auml;nkungen machen, die es verbieten, Ihnen diese Rechte zu verweigern oder Sie aufzufordern, auf diese Rechte zu verzichten. Aus diesen Einschr&auml;nkungen leiten 
sich bestimmte Verpflichtungen f&uuml;r Sie ab, wenn Sie Kopien der Software verbreiten oder diese ver&auml;ndern.</para>
    <para>
      So m&uuml;ssen Sie allen Empf&auml;ngern von Kopien eines solchen Programms die gleichen Rechte einr&auml;umen, die auch Sie selbst haben - unabh&auml;ngig davon, ob Sie das Programm kostenlos oder gegen Bezahlung weitergeben. 
Sie m&uuml;ssen auch sicherstellen, da&szlig; die Empf&auml;nger den Quellcode erhalten bzw. auf Wunsch erhalten k&ouml;nnen. Zudem m&uuml;ssen Sie diese Bedingungen weitergeben, damit die Empf&auml;nger ihre Rechte gem&auml;&szlig; 
dieser Lizenz kennen.</para>
    <para>
      Wir sch&uuml;tzen Ihre Rechte in zwei Schritten: (1) wir stellen die Software unter ein Copyright und (2) bieten Ihnen diese Lizenz an, die Ihnen das Recht gibt, die Software zu vervielf&auml;ltigen, zu verbreiten und/oder 
zu ver&auml;ndern.</para>
    <para>
      Um die Autoren und uns zu sch&uuml;tzen, wollen wir ausdr&uuml;cklich klarstellen, da&szlig; f&uuml;r diese freie Software keine Garantie besteht. Wenn die Software von jemandem ver&auml;ndert und weitergegeben wird, 
m&uuml;ssen die Empf&auml;nger wissen, da&szlig; sie nicht die Originalfassung erhalten haben und somit der Autor der urspr&uuml;nglichen Fassung nicht f&uuml;r durch diese &Auml;nderungen hervorgerufene Probleme verantwortlich 
ist.</para>
    <para>
      Schlie&szlig;lich ist jedes freie Programm dauernd durch Software-Patente bedroht. Wir m&ouml;chten die Gefahr ausschlie&szlig;en, da&szlig; Distributoren von freien Programmen Patente individuell lizensieren und dadurch 
propriet&auml;re Rechte an der Software erwerben. Um dies zu verhindern, haben wir klar festgelegt, da&szlig; jedes Patent f&uuml;r die freie Benutzung durch jedermann lizensiert werden mu&szlig; oder &uuml;berhaupt nicht 
lizensiert werden darf.</para>
    <para>
      Im folgenden werden die genauen Bedingungen f&uuml;r das Kopieren, Verbreiten und Bearbeiten aufgef&uuml;hrt.</para>
    <bridgehead>
      BEDINGUNGEN F&Uuml;R DIE VERVIELF&Auml;LTIGUNG, VERBREITUNG UND BEARBEITUNG
    </bridgehead>
    <orderedlist>	
      <listitem>
      <para>
      Diese Lizenz gilt f&uuml;r jedes Programm oder jedes andere Werk, in dem ein entsprechender Hinweis des Copyright-Inhabers darauf hinweist, da&szlig; seine Arbeit unter den Bedingungen dieser General Public License verbreitet 
werden darf. Im folgenden Text wird jedes Programm bzw. jedes Werk als &quot;Programm&quot; bezeichnet, und die Formulierung &quot;auf dem Programm basierendes Werk&quot; bezieht sich auf das Programm sowie jegliche davon 
abgeleiteten Programme im Sinne des Urheberrechts. Dies bezieht sich auch auf Werke, die dieses Programm oder einen Teil davon, unver&auml;ndert oder ver&auml;ndert, und/oder in eine andere Sprache &uuml;bersetzt, enthalten. 
(Im folgenden ist der Begriff &quot;&Uuml;bersetzung&quot; ohne Einschr&auml;nkung in der Formulierung &quot;Bearbeitung&quot; enthalten). Jeder Lizenznehmer wird im folgenden als &quot;Sie&quot; angesprochen.
</para>
    <para>
      Diese Lizenz behandelt keine anderen Aktivit&auml;ten als Vervielf&auml;ltigung, Verbreitung und Bearbeitung; alle anderen Aktivit&auml;ten fallen nicht in ihren Anwendungsbereich. Der Vorgang der Ausf&uuml;hrung des 
Programms wird nicht eingeschr&auml;nkt, und die Ausgabe des Programms unterliegt der Lizenz nur, wenn der Inhalt ein auf dem Programm basierendes Werk darstellt (unabh&auml;ngig davon, ob die Ausgabe durch die Ausf&uuml;hrung 
des Programms erfolgte). Ob dies zutrifft, h&auml;ngt von den Funktionen des Programms ab.</para>
      </listitem>
      <listitem>
    <para>
      Sie d&uuml;rfen auf beliebigen Medien unver&auml;nderte Kopien des Quellcodes des Programms, wie sie ihn erhalten haben, anfertigen und verbreiten. Voraussetzung hierf&uuml;r ist jedoch, da&szlig; Sie auf jeder Kopie 
einen entsprechenden Copyright-Hinweis sowie einen Haftungsausschlu&szlig; ver&ouml;ffentlichen. Bitte lassen Sie alle Vermerke auf diese Lizenz und auf das Fehlen einer Garantie unver&auml;ndert und geben Sie allen weiteren 
Empf&auml;ngern des Programms eine Kopie dieser Lizenz.</para>
    <para>
      Sie d&uuml;rfen f&uuml;r den eigentlichen Kopiervorgang eine Geb&uuml;hr verlangen und als Gegenleistung eine Garantie f&uuml;r das Programm anbieten, wenn Sie dies w&uuml;nschen.
</para>
      </listitem>
      <listitem>
    <para>
      Sie d&uuml;rften Ihre Kopie bzw. Ihre Kopien des Programms oder eines Teils davon ver&auml;ndern, wodurch ein auf dem Programm beruhendes Werk entsteht. Sie d&uuml;rfen diese Bearbeitungen unter den Bestimmungen des Abschnitts 
1 vervielf&auml;ltigen und verbreiten, wenn Sie zus&auml;tzlich die folgenden Bedingungen erf&uuml;llen:</para>
    <orderedlist>
      <listitem>
	<para>
	  Sie m&uuml;ssen alle ver&auml;nderten Dateien mit einem deutlichen Vermerk versehen, der auf die von Ihnen vorgenommene Bearbeitung und das Bearbeitungsdatum hinweist.
</para>
      </listitem>
      <listitem>
	<para>
	  Sie m&uuml;ssen daf&uuml;r sorgen, da&szlig; jede von Ihnen verbreitete oder ver&ouml;ffentlichte Arbeit, die ganz oder teilweise von einem Programm oder Teilen davon abgeleitet ist, Dritten gegen&uuml;ber als Ganzes unter 
den Bedingungen dieser Lizenz ohne Lizenzgeb&uuml;hren zur Verf&uuml;gung gestellt wird.</para>
      </listitem>
      <listitem>
	<para>
	  Wenn das ver&auml;nderte Programm normalerweise bei der Ausf&uuml;hrung interaktiv Befehle einliest, m&uuml;ssen Sie daf&uuml;r sorgen, da&szlig; das Programm beim Starten auf dem &uuml;blichen Wege eine Meldung ausgibt 
oder ausdruckt, die einen geeigneten Copyright-Vermerk enth&auml;lt sowie einen Hinweis, da&szlig; es keine Gew&auml;hrleistung gibt (oder da&szlig; Sie eine entsprechende Garantie anbieten, falls Sie dies w&uuml;nschen) 
und da&szlig; die Benutzer das Programm unter diesen Bedingungen weiterverbreiten d&uuml;rfen. Zudem mu&szlig; der Benutzer darauf hingewiesen werden, wie er eine Kopie dieser Lizenz anzeigen kann. (Ausnahme: Wenn das Programm 
interaktiv arbeitet, aber normalerweise keine solche Meldung ausgibt, mu&szlig; Ihr auf dem Programm basierendes Werk auch keine solche Meldung ausgeben).
</para>
      </listitem>
    </orderedlist>
    <para>
      Diese Anforderungen betreffen das ver&auml;nderte Werk als Ganzes. Wenn klar unterscheidbare Teile des Werks nicht von dem Programm abgeleitet sind und in sinnvoller Weise als unabh&auml;ngige und eigenst&auml;ndige Werke 
betrachtet werden k&ouml;nnen, erstreckt sich diese Lizenz mit ihren Bedingungen nicht auf diese Abschnitte, wenn Sie diese als eigenst&auml;ndige Werke verbreiten. Wenn Sie jedoch dieselben Abschnitte als Teil eines Ganzen 
verbreiten, das ein auf dem Programm basierendes Werk darstellt, dann mu&szlig; die Verbreitung des Ganzen nach den Bedingungen dieser Lizenz erfolgen. Hierbei werden die Rechte weiterer Lizenznehmer auf das Ganze ausgedehnt 
und damit auf auf jeden einzelnen Teil - unabh&auml;ngig von der Person des Verfassers.</para>
    <para>
      Somit ist es nicht Absicht dieses Abschnitts, Rechte f&uuml;r Werke in Anspruch zu nehmen oder einzuschr&auml;nken, die komplett von Ihnen geschrieben wurden. Die Absicht besteht vielmehr darin, die Rechte zur Kontrolle 
der Verbreitung von Werken, die auf dem Programm basieren oder unter Verwendung von Ausz&uuml;gen dieses Programms zusammengestellt wurden, auszu&uuml;ben.
</para>
    <para>
      Ferner hat ein einfaches Zusammenstellen eines anderen Werks, das nicht auf dem Programm basiert, mit dem Programm (oder mit einem Werk, das auf dem Programm basiert) auf einem Speicher- oder Vertriebsmedium nicht zur Folge, 
da&szlig; diese Lizenz gilt.</para>
      </listitem>
      <listitem>
    <para>
      Sie d&uuml;rfen das Programm (oder ein gem&auml;&szlig; Abschnitt 2 darauf basierendes Werk) als Objektcode oder in ausf&uuml;hrbarer Form unter den Bedingungen von Abschnitt 1 und 2 vervielf&auml;ltigen und verbreiten, 
wenn Sie dabei eine der folgenden Leistungen erbringen:</para>
    <orderedlist>
      <listitem>
	<para>
	  Sie stellen den vollst&auml;ndigen zugeh&ouml;rigen maschinenlesbaren Quellcode auf einem Medium bereit, das &uuml;blicherweise f&uuml;r den Datenaustausch verwendet wird, und verteilen diesen gem&auml;&szlig; den Bedingungen 
der Abschnitte 1 und 2; oder</para>
      </listitem>
      <listitem>
	<para>
	  Sie liefern das Programm mit einem mindestens drei Jahre g&uuml;ltigen schriftlichen Angebot, jedem Dritten eine vollst&auml;ndige maschinenlesbare Kopie des Quellcodes zur Verf&uuml;gung zu stellen, wobei keine weiteren 
Kosten als f&uuml;r den physikalischen Kopiervorgang anfallen und der Quellcode unter den Bedingungen der Abschnitte 1 und 2 auf einem Medium verbreitet wird, das &uuml;blicherweise f&uuml;r den Datenaustausch verwendet wird; 
oder</para>
      </listitem>
      <listitem>
	<para>
	  Sie liefern das Programm mit der Information, die auch Sie erhalten haben, da&szlig; der zugeh&ouml;rige Quellcode angeboten wird. (Diese Alternative ist nur zul&auml;ssig, wenn Sie das Programm ohne kommerzielle Interessen 
weitergeben und das Programm als Objektcode oder in ausf&uuml;hrbarer Form mit einem entsprechenden Angebot nach Unterabschnitt b erhalten haben.)
</para>
      </listitem>
      </orderedlist>
    <para>
      Unter Quellcode eines Werks wird die Form des Werks verstanden, die f&uuml;r Bearbeitungen vorzugsweise verwendet wird. F&uuml;r ein ausf&uuml;hrbares Programm besteht der vollst&auml;ndige Quellcode aus dem Quellcode 
f&uuml;r alle enthaltenen Module des Programms, den zugeh&ouml;rigen Dateien f&uuml;r die Schnittstellendefinition und Skripten, die das Kompilieren und Installieren der ausf&uuml;hrbaren Programmteile steuern. Davon ausdr&uuml;cklich 
ausgenommen sind Komponenten, die normalerweise (als Quellcode oder in bin&auml;rer Form) mit den Hauptkomponenten des Betriebssystems wie Compiler und Kernel verteilt werden, unter dem das Programm ausgef&uuml;hrt wird. Dies 
gilt jedoch nicht f&uuml;r Komponenten, die zum ausf&uuml;hrbaren Programm geh&ouml;ren.</para>    
    <para>
      Wenn die Verbreitung eines ausf&uuml;hrbaren Programms oder des Objektcodes erfolgt, indem diese Komponenten an einem bestimmten Ort f&uuml;r das Kopieren zur Verf&uuml;gung gestellt werden, so gilt die Gew&auml;hrung 
der M&ouml;glichkeit zum gleichzeitigen Zugreifen auf den Quellcode als Verbreitung des Quellcodes, auch wenn Dritte nicht dazu verpflichtet sind, die Quelle mit dem zugeh&ouml;rigen Objektcode zu kopieren.
</para>
      </listitem>
      <listitem>
    <para>
      Sie d&uuml;rfen das Program nicht kopieren, &auml;ndern, unterlizensieren oder verbreiten, wenn dies durch diese Lizenz nicht ausdr&uuml;cklich gestattet wird. Jeder anderweitige Versuch der Vervielf&auml;ltigung, &Auml;nderung, 
Weiterlizensierung und Verbreitung ist nicht zul&auml;ssig und hat automatisch den Verlust Ihrer Rechte gem&auml;&szlig; dieser Lizenz zur Folge. Die Lizenzen Dritter, die Kopien oder Rechte unter dieser Lizenz erhalten haben, 
werden jedoch nicht beendet, solange sie diese Lizenz in vollem Umfang anerkennen und befolgen.</para>
      </listitem>
      <listitem>
    <para>
      Sie sind nicht verpflichtet, diese Lizenz zu akzeptieren, da Sie sie nicht unterzeichnet haben. Diese Lizenz ist jedoch das einzige Dokument, das Ihnen das Recht zum &Auml;ndern oder Verbreiten des Programms oder davon abgeleiteten 
Werken einr&auml;umt. Diese Aktionen sind gesetzlich verboten, wenn Sie diese Lizenz nicht anerkennen. Wenn Sie das Programm (oder davon abgeleitete Werke) also &auml;ndern oder verbreiten, erkl&auml;ren Sie damit Ihr Einverst&auml;ndnis 
mit dieser Lizenz und allen ihren Bedingungen bez&uuml;glich der Vervielf&auml;ltigung, Verbreitung und Ver&auml;nderung des Programms oder eines darauf basierenden Werks.
</para>
      </listitem>
      <listitem>
    <para>
      Jedesmal, wenn Sie das Programm (oder ein auf dem Programm basierendes Werk) weitergeben, erh&auml;lt der Empf&auml;nger automatisch eine Lizenz vom urspr&uuml;nglichen Lizenznehmer zum Kopieren, Verbreiten oder Ver&auml;ndern 
gem&auml;&szlig; den hier festgelegten Bestimmungen. Sie d&uuml;rfen keine weiteren Einschr&auml;nkungen der Durchsetzung der hierin zugestandenen Rechte des Empf&auml;ngers vornehmen. Sie sind nicht daf&uuml;r verantwortlich, 
die Einhaltung dieser Lizenz durch Dritte durchzusetzen.</para>
      </listitem>
      <listitem>
    <para>
      Sollten Ihnen in Folge eines Gerichtsurteils, des Vorwurfs einer Patentverletzung oder aus anderen Gr&uuml;nden (nicht auf Patentfragen begrenzt) Bedingungen (durch Gerichtsbeschlu&szlig;, Vergleich oder anderweitig) auferlegt 
werden, die den Bedingungen dieser Lizenz widersprechen, so entbinden Sie diese Umst&auml;nde nicht von den Bedingungen dieser Lizenz. Wenn es Ihnen nicht m&ouml;glich ist, das Programm unter gleichzeitiger Beachtung der Bedingungen 
in dieser Lizenz und Ihrer weiteren Verpflichtungen zu verbreiten, d&uuml;rfen Sie folglich das Programm &uuml;berhaupt nicht mehr verbreiten. Wenn beispielsweise ein Patent nicht die Weiterverbreitung des Programms ohne Geb&uuml;hren 
durch diejenigen erlaubt, die das Programm direkt oder indirekt von Ihnen erhalten haben, dann besteht der einzige Weg, das Patent und diese Lizenz zu befolgen, darin, ganz auf die Verbreitung des Programms zu verzichten.
</para>
    <para>
      Sollte sich ein Teil dieses Abschnitts als ung&uuml;ltig oder unter bestimmten Umst&auml;nden nicht durchsetzbar erweisen, so mu&szlig; dieser Abschnitt gem&auml;&szlig; der zugrundeliegenden Intention angewendet werden. 
In den &uuml;brigen F&auml;llen gilt dieser Abschnitt jedoch als Ganzes.</para>
    <para>
      Es liegt nicht in der Absicht dieses Abschnitts, Sie zur Verletzung von Patenten oder anderen Eigentumsanspr&uuml;chen zu verleiten oder die G&uuml;ltigkeit solcher Anspr&uuml;che zu bestreiten. Dieser Abschnitt hat ausschlie&szlig;lich 
den Zweck, das Funktionieren des Distributionssystems f&uuml;r freie Software zu sch&uuml;tzen, das auf der Verwendung &ouml;ffentlicher Lizenzen beruht. Zahlreiche Personen haben auf gro&szlig;z&uuml;gige Weise Beitr&auml;ge 
zu der vielf&auml;ltigen Software geleistet, die auf diese Weise verbreitet wird, und dabei auf die Einhaltung dieser Bestimmungen vertraut. Die Entscheidung, ob Software mittels dieses oder eines anderen Systems verbreitet werden 
soll, obliegt ausschlie&szlig;lich dem Autor/Geber der Software. Der Lizenznehmer hat auf diese Entscheidung keinen Einflu&szlig;.
</para>
    <para>
      In diesem Abschnitt soll eindeutig und klar dargestellt werden, was als Schlu&szlig;folgerung aus dem restlichen Inhalt dieser Lizenz betrachtet wird.
</para>    
      </listitem>
      <listitem>
    <para>
      Wenn die Verbreitung und/oder Verwendung des Programms in bestimmten Staaten entweder durch Patente oder urheberrechtlich gesch&uuml;tzte Schnittstellen eingeschr&auml;nkt ist, kann der Inhaber des Urheberrechts, der das 
Programm unter diese Lizenz gestellt hat, f&uuml;r die geografische Verbreitung der Software eine Beschr&auml;nkung festlegen, in der diese Staaten ausgeschlossen werden und die Verbreitung nur in oder zwischen den Staaten erlaubt 
ist, f&uuml;r die diese Beschr&auml;nkung nicht gilt. In diesem Fall beinhaltet diese Lizenz die Beschr&auml;nkung, als ob sie in diesem Text schriftlich enthalten w&auml;re.
</para>
      </listitem>
      <listitem>
    <para>
      Die Free Software Foundation kann von Zeit zu Zeit &uuml;berarbeitete und/oder neue Versionen der General Public License ver&ouml;ffentlichen. Diese neuen Versionen werden der Intention der vorliegenden Version folgen, 
k&ouml;nnen aber in Details abweichen, um neuen Problemen oder Anforderungen gerecht zu werden.</para>
    <para>
      Jede Version verf&uuml;gt &uuml;ber eine eindeutige Versionsnummer. Wenn im Programm ein Hinweis auf eine bestimmte Version enthalten ist, versehen mit dem Zusatz, auch sp&auml;tere Versionen seien anzuwenden, so k&ouml;nnen 
Sie frei w&auml;hlen, ob Sie den Bestimmungen dieser Version oder einer sp&auml;teren Version folgen m&ouml;chten, die von der Free Software Foundation ver&ouml;ffentlicht wurde. Wenn im Programm keine bestimmte Versionsnummer 
dieser Lizenz angegeben wurde, k&ouml;nnen Sie eine beliebige Version w&auml;hlen, die von der Free Software Foundation ver&ouml;ffentlicht wurde.
</para>
      </listitem>
      <listitem>
    <para>
      Wenn Sie Teile des Programms in anderen freien Programmen verwenden m&ouml;chten, die anderen Bestimmungen f&uuml;r die Verbreitung unterliegen, sollten Sie sich an den Autor wenden, um ihn um die Erlaubnis zu bitten. Bei 
Software, die dem Copyright der Free Software Foundation unterliegt, schreiben Sie an die Free Software Foundation; wir machen hierzu manchmal Ausnahmen. F&uuml;r unsere Entscheidung spielen dabei zwei Faktoren eine Rolle: So 
soll der freie Status von allen Programmen, die von unserer freien Software abgeleitet werden, erhalten bleiben sowie die Verbreitung und Weiterverwendung von Software generell gef&ouml;rdert werden.
</para>
    <bridgehead>
      KEINE GEW&Auml;HRLEISTUNG</bridgehead>
      </listitem>
      <listitem>
    <para>
      DA DAS PROGRAMM OHNE JEGLICHE KOSTEN LIZENSIERT WIRD, BESTEHT KEINERLEI GEW&Auml;HRLEISTUNG F&Uuml;R DAS PROGRAMM, SOWEIT DIES GESETZLICH ZUL&Auml;SSIG IST. SOFERN NICHT ANDERWEITIG SCHRIFTLICH BEST&Auml;TIGT, STELLEN 
DIE COPYRIGHT-INHABER UND/ODER DRITTE DAS PROGRAMM SO ZU VERF&Uuml;GUNG, &quot;WIE ES IST&quot;, OHNE IRGENDEINE GEW&Auml;HRLEISTUNG, WEDER AUSDR&Uuml;CKLICH NOCH IMPLIZIT, EINSCHLIESSLICH, ABER NICHT BEGRENZT AUF TAUGLICHKEIT 
UND VERWENDBARKEIT F&Uuml;R EINEN BESTIMMTEN ZWECK. DAS VOLLE RISIKO BEZ&Uuml;GLICH QUALIT&Auml;T UND LEISTUNGSF&Auml;HIGKEIT DES PROGRAMMS LIEGT BEI IHNEN. SOLLTE DAS PROGRAMM FEHLERHAFT SEIN, &Uuml;BERNEHMEN SIE DIE KOSTEN 
F&Uuml;R NOTWENDIGEN SERVICE, REPARATUR ODER KORREKTUR.</para>
      </listitem>
      <listitem>
    <para>
      IN KEINEM FALL, AUSSER DURCH GELTENDES RECHT GEFORDERT ODER SCHRIFTLICH ZUGESICHERT, IST IRGENDEIN COPYRIGHT-INHABER ODER IRGENDEIN DRITTER, DER DAS PROGRAMM WIE OBEN ERLAUBT MODIFIZIERT ODER VERBREITET HAT, IHNEN GEGEN&Uuml;BER 
F&Uuml;R IRGENDWELCHE SCH&Auml;DEN HAFTBAR, EINSCHLIESSLICH JEGLICHER GENERELLER, SPEZIELLER, ZUF&Auml;LLIGER ODER FOLGESCH&Auml;DEN, DIE AUS DER BENUTZUNG DES PROGRAMMS ODER DER UNBENUTZBARKEIT DES PROGRAMMS FOLGEN (EINSCHLIESSLICH, 
ABER NICHT BESCHR&Auml;NKT AUF DATENVERLUSTE, FEHLERHAFTE VERARBEITUNG VON DATEN, VERLUSTE, DIE VON IHNEN ODER ANDEREN GETRAGEN WERDEN M&Uuml;SSEN, ODER EINEN FEHLER DES PROGRAMMS BEIM ZUSAMMENARBEITEN MIT EINEM ANDEREN PROGRAMM), 
SELBST WENN EIN COPYRIGHT-INHABER ODER DRITTER &Uuml;BER DIE M&Ouml;GLICHKEIT SOLCHER SCH&Auml;DEN UNTERRICHTET WORDEN WAR.
</para>
      </listitem>
      </orderedlist>
    <para>
      ENDE DER BEDINGUNGEN
    </para>
   </appendix>