%define  ver     1.1.9
%define  rel     5
%define  prefix  /usr

Summary: GNOME User's Guide
Name: gnome-users-guide
Version: %ver
Release: %rel
Copyright: GPL
Group: Documentation
Source: users-guide-%{PACKAGE_VERSION}.tar.gz
BuildRoot:/var/tmp/gnome-users-guide-%{PACKAGE_VERSION}-root
Docdir: %{prefix}/doc
BuildArchitectures: noarch

%description
User's Guide for the GNOME Desktop Environment

%prep
%setup -n users-guide-%{PACKAGE_VERSION}

%build
./configure --prefix=%{prefix}

%install

rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT
make prefix=$RPM_BUILD_ROOT/%{prefix} install 

%clean
rm -rf $RPM_BUILD_ROOT

%changelog

* Tue Feb 28 1999 David Mason <dcm@redhat.com>
- version 1.1

* Mon Feb 08 1999 Michael Fulbright <drmike@redhat.com>
- version 0.99.5

* Thu Feb 04 1999 Michael Fulbright <drmike@redhat.com>
- version 0.99.3

* Mon Jan 18 1999 Michael Fulbright <drmike@redhat.com>
- version 0.99.2

* Thu Dec 17 1998 Michael Fulbright <drmike@redhat.com>
- first pass at a spec file

%files
%defattr(-, root, root)
%doc README ChangeLog 
%{prefix}/share/gnome/help/users-guide
