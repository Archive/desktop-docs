
<!-- 참고: CVS revision 1.19에 대한 번역판임. 원본의 update가 발생한
  ==       경우에는 위 revision에 대해 diff를 돌려서 같이 수정할 것
  ==
  == 번역: Sung-Hyun Nam <namsh@kldp.org>
  -->

<!-- #################### GNOME APPLICATIONS - PART ####################### 

<part id="gnomeapps">
 <title>GNOME Applications </title>
 <partintro>
  <para>
  <indexterm id="idx-b1"
   <primary>
    GNOME Applications
   </primary>
  </indexterm>
   There are  many GNOME applications that are  included with the
   packages used  to install GNOME.  There are more  GNOME applications
   being written on  a daily basis that are  considered GNOME compliant
   but  are  not included  with  the  distribution.  This section  will
   contain both types of GNOME  applications but does not contain
   all  of  them. Some GNOME applications, particularly large ones, may
   come with their own separate documentation. Check the package you
   used to install that application if it is not described in this
   section.
 </para>
 </partintro> -->

<!-- #################### PANEL APPLETS - CHAPTER ####################### -->

 <chapter id="panelapps">
  <title>패널 애플릿</title>
  <sect1>
  <title>소개</title>
   <para>
   <indexterm id="idx-b2">
    <primary>
     패널
    </primary>
    <secondary>
     애플릿
    </secondary><see>"애플릿"</see>
   </indexterm>
   <indexterm>
    <primary>
     애플릿
    </primary>
   </indexterm>
    이 섹션에서는 그놈 패널에 추가할 수 있는 그놈 애플릿들에 대해 설명한다. 이
    애플릿들을 사용하려면 패널에서 오른쪽 마우스를 누른 후 <guimenuitem>새
    애플릿 추가</guimenuitem>를 <guimenu>팝업</guimenu> 메뉴에서 선택한다.
   </para>
  </sect1>

<!-- ######### AMUSEMENT APPLETS - SECTION ########## -->

   <sect1 id="appletsamuse">
    <title>오락</title>
    <para>
    <indexterm>
     <primary>
      애플릿
     </primary>
     <secondary>
      오락
     </secondary>
    </indexterm>
     오락 애플릿은 순전히 당신의 재미를 위해 시간과 자원을 낭비해서 당신의
     인생을 좀더 부유하게 만들도록 개발되었습니다. 당신의 두뇌의 가치있는
     공간이 오락 애플릿들이 가르쳐주는 어떤 것들로 채워지는 걸 느끼게 된다면
     그 애플릿들의 개발자들은 성공했다고 할 수 있습니다.
    </para>
   </sect1>

<!-- ######### MONITOR APPLETS - SECTION ########## -->

   <sect1 id="appletsmon">
    <title>감시기</title>
    <para>
    <indexterm>
     <primary>
      애플릿
     </primary>
     <secondary>
      감시기
     </secondary>
    </indexterm>
     감시 애플릿은 당신의 시스템과 그 기능들을 감시하도록 개발되었습니다. 당신
     기계의 자원들을 감시할 수 있습니다.
    </para>

<!-- ######## BATTERY MONITOR - APPLET ########## -->

   <sect2 id="appletsbatmon">
    <title>건전지 감시</title>
    <para>
    <indexterm>
     <primary>
      애플릿
     </primary>
     <secondary>
      건전지 감시
     </secondary>
    </indexterm>
     건전지 감시기는 간단한 애플릿으로서 당신의 랩탑 모니터에서 얼마나 더
     작업할 수 있는지를 보여줍니다.
    </para>
   </sect2>

<!-- ######## BATTERY CHARGE MONITOR - APPLET ########## -->

   <sect2 id="appletscharge">
    <title>건전지 충전 감시</title>
    <para>
    <indexterm>
     <primary>
      애플릿
     </primary>
     <secondary>
      건전지 충전 감시
     </secondary>
    </indexterm>
     건전지 충전 감시기는 랩탑의 건전지가 얼마나 충전되어 있는 지, 그리고
     얼마나 더 작업할 수 있는 지를 보여줍니다. 또한 현재 플러그에 꼽혀 있는
     지도 보여줍니다. 애플릿에서 왼쪽 마우스를 클릭하면 수직축은 충전 용량을
     수평축은 남은 시간을 가리키는 그래프로 바뀝니다. 그러므로 랩탑이 충전되고
     있다면 그래프는 올라가야 하고, 건전지를 사용중이라면 그래프는 떨어져야
     합니다.
    </para>
    <para> 
     애플릿에서 오른쪽 마우스를 클릭하면 팝업 메뉴가 보이게 될 겁니다. 속성
     메뉴를 선택하시면 표시되는 색상, 건전지가 거의 방전되었을 때 알려주는
     기능과 애플릿의 일반적인 룩앤필을 편집하실 수 있습니다.
    </para>
   </sect2>

<!-- ######## CPU/MEM USAGE - APPLET ########## -->

   <sect2 id="appletsusage" >
    <title>중앙처리장치/기억장치 사용량</title>
    <para>
    <indexterm>
     <primary>
      애플릿
     </primary>
     <secondary>
      중앙처리장치/기억장치 사용량
     </secondary>
    </indexterm>
     중앙처리장치/기억장치 사용량 감시기는 현재의 CPU, 기억장치, 스왑 공간의
     사용량을 보여줍니다. 이 애플릿은 사용량을 색으로 보여주는 3개의 막대를
     가지고 있습니다. 수평 패널에서 이 애플릿을 실행시키고 있다면 제일 위가
     중앙처리장치, 가운데 막대가 기억장치, 그리고 제일 아래 막대가 스왑 공간을
     나타냅니다. 수직 패널에서 실행시키고 있다면 중앙처리장치가 왼쪽,
     기억장치가 중간, 그리고 스왑 공간이 오른쪽 막대입니다.
    </para>
     <figure>
      <title>중앙처리장치/기억장치 애플릿</title>
     <screenshot>
      <screeninfo>중앙처리장치/기억장치 애플릿</screeninfo>
      <Graphic Format="gif" Fileref="./figs/cpumem-applet" srccredit="dcm">
      </graphic>
     </screenshot>
     </figure>
    <ITEMIZEDLIST MARK="bullet">
     <listitem>
      <para>중앙처리장치 - 세가지의 색으로 현재 중앙처리장치의 사용량을
      구분해서 표시합니다. 황색은 현재 사용자에 의한 사용량을, 회색은
      시스템에서 활동중이라고 명시된 비사용에 의한 사용량을, 그리고 흑색은
      남은 용량을 나타냅니다.
      </para>
     </listitem>
     <listitem>
      <para>
       기억장치 - 기억장치 막대는 네가지 색중 하나로 현재의 물리적인 기억장치
       사용량을 표시합니다. 황색은 공유 메모리. 회색-황색은 기타 메모리.
       회색은 사용중인 버퍼. 녹색은 사용 가능한 물리적인 메모리의 양을
       표시합니다.
      </para>
     </listitem>
     <listitem>
      <para>
       스왑 공간 - 스왑 공간 막대는 황색으로 얼마나 많은 스왑 공간이 사용중인
       지를 표시합니다. 남은 공간은 녹색으로 표시됩니다.
      </para>
     </listitem>
    </itemizedlist>
   </sect2>

<!-- ######### DISK USAGE - APPLET ######### 
   <sect2 id="appletsdu">
    <title>Disk Usage Applet</title>
     <figure>
      <title>The Disk Usage Applet</title>
     <screenshot>
      <screeninfo>The Disk Usage Applet</screeninfo>
      <Graphic Format="gif" Fileref="./figs/diskuse-applet" srccredit="dcm">
      </graphic>
     </screenshot>
     </figure>
     <figure>
      <title>The Disk Usage Applet Properties</title>
     <screenshot>
      <screeninfo>The Disk Usage Applet Properties</screeninfo>
      <Graphic Format="gif" Fileref="./figs/diskuse-applet-props" srccredit="dcm">
      </graphic>
     </screenshot>
     </figure>
   </sect2> -->

<!-- ######### CPULOAD - APPLET ######### -->

   <sect2 id="appletscpu">
    <title>중앙처리장치 로드 애플릿</title>
    <para>
    <indexterm>
     <primary>
      애플릿
     </primary>
     <secondary>
      중앙처리장치 로드
     </secondary>
    </indexterm>
     중앙처리장치 로드 애플릿은 세가지의 색을 이용하여 중앙처리장치의 사용량을
     보여드립니다.
    </para>
     <figure>
      <title>중앙처리장치 로드 애플릿</title>
      <screenshot>
       <screeninfo>중앙처리장치 로드 애플릿</screeninfo>
       <Graphic Format="gif" Fileref="./figs/CPULoad" srccredit="dcm">
       </graphic>
      </screenshot>
     </figure>
    <para>
     황색은 현재 사용자에 의한 사용량을, 회색은 시스템에서 활동중이라고 명시된
     비사용에 의한 사용량을, 그리고 흑색은 남은 용량을 나타냅니다.
    </para>
   </sect2> 

<!-- ######### MEMLOAD - APPLET ######### -->

   <sect2 id="appletsmem">
    <title>기억장치 로드 애플릿</title>
    <para>
    <indexterm>
     <primary>
      애플릿
     </primary>
     <secondary>
      MEMLoad
     </secondary>
    </indexterm>
      기억장치 로드는 네가지의 색을 사용하여 현재 기억장치의 사용량을
      보여드립니다.
    </para>
<figure>
      <title>기억장치 로드 애플릿</title>
      <screenshot>
       <screeninfo>기억장치 로드 애플릿</screeninfo>
       <Graphic Format="gif" Fileref="./figs/MEMLoad" srccredit="dcm">
       </graphic>
      </screenshot>
     </figure>
    <para>
     황색은 공유 메모리. 회색-황색은 기타 메모리. 회색은 사용중인 버퍼. 녹색은
     사용 가능한 물리적인 메모리의 양을 표시합니다.
    </para>
   </sect2> 

<!-- ######### SWAPLOAD - APPLET ######### -->

   <sect2 id="appletsswap">
    <title>스왑 로드 애플릿</title>
    <para>
    <indexterm>
     <primary>
      애플릿
     </primary>
     <secondary>
      SWAPLoad
     </secondary>
    </indexterm>
     스왑 로드는 두가지의 색을 이용하여 스왑 공간의 사용량을 보여드립니다.
    </para>
     <figure>
      <title>스왑 로드 애플릿</title>
      <screenshot>
       <screeninfo>스왑 로드 애플릿</screeninfo>
       <Graphic Format="gif" Fileref="./figs/SWAPLoad" srccredit="dcm">
       </graphic>
      </screenshot>
     </figure>
    <para>
     스왑 공간 막대는 황색으로 얼마나 많은 스왑 공간이 사용중인 지를
     표시합니다. 남은 공간은 녹색으로 표시됩니다.
    </para>
   </sect2>


<!-- ######### NETLOAD - APPLET ######### 

   <sect2 id="appletsnetload">
    <title>NETLoad Applet</title>
    <para>
    <indexterm>
     <primary>
      애플릿
     </primary>
     <secondary>
      NETLoad
     </secondary>
    </indexterm>
     The NETLoad  is a simple graph  that shows you  the current Net
     Space usage using one of three colors:
    </para>
     <figure>
      <title>The NETLoad Applet</title>
      <screenshot>
       <screeninfo>The NETLoad Applet</screeninfo>
       <Graphic Format="gif" Fileref="./figs/NETLoad" srccredit="esr">
       </graphic>
      </screenshot>
     </figure>
    <para>
    The grey bar (if present) shows PPP activity.  The blue bar (if
    present) shows SLIP activity.  (You're not likely to see both 
    of these together.) The white bar (if present) shows any other
    network activity, such as local-area-network traffic.
    </para>
    <para>
    The scale of the NETload graph adjusts itself according to the
    highest activity level seen since startup; think of it as a 
    network saturation meter.  Traffic through the
    loopback device (used for TCP/IP between programs local to your
    machine) is excluded, so you should see only traffic with other
    network hosts.
    </para>
   </sect2> -->
 </sect1>

<!-- ######### MULTIMEDIA APPLETS - SECTION ########## -->

  <sect1 id="appletsmm">
    <title>멀티미디어</title>
    <para>
    <indexterm>
     <primary>
      애플릿
     </primary>
     <secondary>
      멀티미디어
     </secondary>
    </indexterm>
     멀티미디어 애플릿은 시스템에서 멀티미디어를 사용할 수 있는 애플릿들을
     모아놓은 것입니다. 소리, 동영상과 다른 멀티미디어들을 조작할 수 있는 
     애플릿들을 볼 수 있을 겁니다.
    </para>

<!-- ######### CD PLAYER - APPLET ########## -->

   <sect2 id="appletscd">
    <title>CD 재생 애플릿</title>
    <para>
    <indexterm>
     <primary>
      애플릿
     </primary>
     <secondary>
      CD 재생
     </secondary>
    </indexterm>
     CD 재생 애플릿은 패널 안에 놓일 수 있는 간단한 CD 재생기입니다.
    </para>
    <para>
     CDROM안에 오디오 CD를 넣으셨다면 재생/중지, 멈춤, 전진, 후진과 꺼냄
     버튼등을 사용하여 CD를 조작할 수 있습니다. 버튼 위에는 트랙의 남은 시간을
     보여주고 전진과 후진 버튼 사이의 숫자는 트랙 번호입니다.
    </para>
    <figure>
     <title>CD 재생 애플릿</title>
     <screenshot>
      <screeninfo>CD 재생 애플릿</screeninfo>
      <Graphic Format="gif" Fileref="./figs/cdapplet" srccredit="dcm">
      </graphic>
     </screenshot>
    </figure>
    <important>
     <title>중요</title>
     <para>
      이 애플릿을 사용하려면 CDROM 드라이브에 대한 접근 권한을 가지고 있어야
      합니다. 어떤 시스템은 당신이 콘솔로 로그인하면 자동으로 권한을 부여하는
      데, 이런 메카니즘은 <application>pam_console</application>이라 불립니다.
      당신의 시스템이 이런 권한을 주지 못하는 경우 그런 권한을 얻도록 해야
      합니다. 루트 비밀번호를 아신다면 터미널 창에서 다음과 같이 하십시요.
      <programlisting>
       $ su 
       $ Password: [루트 비밀번호 입력] 
       $ chmod a+r /dev/cdrom
       $ exit
      </programlisting>
     </para>
     <para>
      CDROM이 /dev/cdrom이 아닌 경우에는 위의 명령어에서 해당되는 cdrom 장치를
      선택하셔야 합니다.
     </para>
    </important>
    <para>
     CD에 대해 더 많은 조작을 원하시면 CD 재생 애플릿에서 오른쪽 마우스를
     클릭하신 후 <guimenu>팝업</guimenu>에서 <guimenuitem>Run
     gtcd...</guimenuitem>를 선택하십시요. 그러면 그놈 CD 재생기가 실행됩니다.
     더 많은 정보를 원하시면 <xref linkend="gtcd">를 읽어보십시요.
    </para>
    <important>
     <title>중요</title>
     <para>
      CD 재생 애플릿이 실행중일 때는 CDROM 드라이브에 있는 꺼냄 버튼을 사용할
      수 없습니다. CD를 꺼내고 싶으시면 CD 재생 애플릿의 꺼냄 버튼을
      사용하셔야 합니다.
     </para>
    </important>
    
   </sect2> 

<!-- ########### MIXER APPLET########## -->

<!--
   <sect2 id="appletsmix">
    <title>Mixer Applet</title>
    <para>
    <indexterm>
     <primary>
      애플릿
     </primary>
     <secondary>
      Mixer
     </secondary>
    </indexterm>
     The Mixer  Applet is a simple  applet that allows  you to control
     the volume  on your  system. There are  two main controls  on the
     applet:  the Volume  Slidebar and  the Mute  button. To  raise or
     lower the volume use the slide  bar with your mouse. To mute your
     system press the small mute button on the bottom of the applet.
    </para>
    <figure>
     <title>The Mixer Applet</title>
     <screenshot>
      <screeninfo>The Mixer Applet</screeninfo>
      <Graphic Format="gif" Fileref="./figs/mixapplet" srccredit="dcm">
      </graphic>
     </screenshot>
    </figure>
    <para>
     If you  want more control over  your system volume  you may right
    mouse click  on the Mixer  Applet and select  the <guimenuitem>Run
    gmix</guimenuitem>  menu item  from  the <guimenu>pop-up</guimenu>
    menu.  </para>
   </sect2>
-->
 </sect1> 

<!-- ######### NETWORK APPLETS - SECTION ########## -->

   <sect1 id="appletsnet">
    <title>네트워크</title>
    <para>
    <indexterm>
     <primary>
      애플릿
     </primary>
     <secondary>
      네트워크
     </secondary>
    </indexterm>
     네트워크 애플릿은 네트워크나 인터넷을 감시하거나 당신의 작업을
     도와줍니다. 네트워크 애플릿은 전자우편이 도착했는 지 확인하는 것부터
     당신이 인터넷에 얼마나 오랫동안 접속했는 지를 감시하는 것까지 다양한
     기능들을 제공합니다.
    </para>

<!-- ########## MAILCHECK - APPLET ######### -->

    <sect2 id="appletsmail">
     <title>편지 확인 애플릿</title>
     <para>
     <indexterm>
     <primary>
      애플릿
     </primary>
     <secondary>
      편지 확인
     </secondary>
    </indexterm>
      편지 확인 애플릿은 시스템이 편지가 와 있는 지를 검사하고 당신에게
      알려줍니다. 이 애플릿은 로컬 시스템에 있는 편지만 확인합니다. 다른
      서버에 있는 편지는 확인하지 않습니다. 편지 확인 애플릿의 속성을 바꾸고
      싶으시면 오른쪽 마우스로 클릭하여 <guimenu>팝업</guimenu> 메뉴의
      <guimenuitem>속성</guimenuitem> 메뉴 항목을 선택하십시요. 편지 확인 속성
      대화상자가 나타나고, 거기에서 다음과 같은 속성들을 바꿀 수 있습니다.
      매번 갱신하기 전에 어떤 프로그램을 실행하고 싶으시면 텍스트 박스에
      명령을 입력하시면 됩니다. 당신이 fetchmail을 실행하여 다른 서버에서
      편지를 가져오고 싶을 때 유용한 기능입니다. 얼마나 자주 확인할 것인지를
      결정할 수 있으며 애플릿의 에니메이션 형식을 선택하실 수도 있습니다.
     </para>
    </sect2>

<!-- ########## WEBCONTROL - APPLET ######### -->

    <sect2 id="appletsweb">
     <title>웹 조정판 애플릿</title>
     <para>
     <indexterm>
     <primary>
      애플릿
     </primary>
     <secondary>
      웹 조정판
     </secondary>
    </indexterm>
      웹 조정판 애플릿은 당신이 URL 텍스트 박스에서 입력한 URL로 웹 브라우져를
      실행합니다. 이미 실행되고 있는 브라우져를 이용하지 않고 새로운 브라우져
      창을 열고 싶으면 Launch new window 확인박스를 선택하시면 됩니다. URL
      텍스트 박스를 지우려면 <guibutton>Clear</guibutton> 버튼을 누르십시요.
      속성을 바꾸려면 오른쪽 마우스로 애플릿을 클릭한 후
      <guimenu>팝업</guimenu> 메뉴에서 <guimenuitem>속성</guimenuitem> 메뉴
      항목을 선택하시면 됩니다. 속성 대화상자에서 URL 텍스트 라벨이 표시될
      것인지와 새 창을 여는 옵션을 바꿀 수 있습니다.
     </para>
    </sect2>

<!-- ########## MODEM LIGHTS - APPLET ######### -->

    <sect2 id="appletsmodem">
     <title>모뎀 깜박이 애플릿</title>
     <para>
      모뎀 깜박이 애플릿을 당신이 dial up 연결을 시작하고 연결된 후에는 감시를
      할 수 있습니다. 이것은 두개의 작은 모뎀 깜박이를 표시하며 모뎀이 사용되고
      있는 상태를 감시합니다.
     </para>
     <para>
      모뎀 깜박이를 설정하려면 당신의 시스템에서 dial up 연결이 동작해야
      합니다. 애플릿에서 오른쪽 마우스를 클릭한 후 <guimenu>팝업</guimenu>
      메뉴에서 <guimenuitem>속성</guimenuitem> 메뉴 항목을 선택하십시요.
      그러면 모뎀 깜박이 설정 대화상자가 열립니다.
     </para>
     <para>
      이 대화상자에는 <guilabel>일반</guilabel>과
      <guilabel>Advanced</guilabel>라는 두개의 탭이 있습니다.
      <guilabel>일반</guilabel> 탭에서는 <guilabel>Connect command</guilabel>
      텍스트 박스에서 dialup 연결을 시작할 스크립트를 입력할 수 있습니다.
      연결끊기 스크립트는 <guilabel>Disconnect command</guilabel> 텍스트
      박스에 입력하시면 됩니다. 적용을 누르신 후 애플릿의 가장자리에 하나만
      있는 작은 버튼을 누르십시요. 그러면 스크립트가 실행될 것 입니다. 한번
      연결이 되면 작은 모뎀 깜박이가 깜박거릴 것입니다. 인터넷 로드는 가운데
      있는 작은 검은 박스에 표시됩니다.
     </para>
    </sect2>
   </sect1>

<!-- ######## UTILITY APPLETS - SECTION ########## -->

   <sect1 id="appletsutil">
    <title>도구</title>
    <para>
    <indexterm>
     <primary>
      애플릿
     </primary>
     <secondary>
      도구
     </secondary>
    </indexterm>
     도구 애플릿은 당신의 작업 환경에서 사용될 수 있는 일반적인 도구들을
     모아놓은 것입니다.
    </para>

<!-- ######### CLOCK - APPLET ######### -->

    <sect2 id="appletsclock">
     <title>시계 애플릿</title>
     <para>
     <indexterm>
     <primary>
      애플릿
     </primary>
     <secondary>
      시계
     </secondary>
    </indexterm>
      시계 애플릿은 그놈이 처음 인스톨되었을 때 자동으로 실행되는 단 하나의
      애플릿입니다. 시계에서 오른쪽 마우스를 클릭하신 후
      <guimenu>팝업</guimenu> 메뉴에서 <guimenuitem>속성</guimenuitem> 메뉴
      항목을 선택하시면 속성을 바꿀 수 있습니다. 속성 대화상자에서는
      12시간 혹은 24시간 단위로 표시할 지를 선택할 수 있습니다.
     </para>
    </sect2>

<!-- ######### PRINTER - APPLET ######### -->

    <sect2 id="appletsprint">
     <title>인쇄기 애플릿</title>
     <para>
     <indexterm>
     <primary>
      애플릿
     </primary>
     <secondary>
      인쇄기
     </secondary>
    </indexterm>
      인쇄기 애플릿을 패널에 작은 인쇄기 아이콘으로 표시됩니다. 파일를 인쇄기
      애플릿으로 끌어오면 그 파일이 인쇄될 것입니다. 인쇄기 애플릿을
      설정하려면 그 위에서 오른쪽 마우스를 클릭한 후 <guimenu>팝업</guimenu>
      메뉴에서 <guimenuitem>속성</guimenuitem> 메뉴 항목을 선택하십시요.
      그러면 인쇄기 속성 대화상자가 열립니다. 이 대화상자에서 인쇄기 이름과
      인쇄 명령을 줄 수 있습니다. 대부분의 시스템에서 인쇄 명령은
      <programlisting>lpr</programlisting>입니다.
     </para>
     <figure>
      <title>인쇄기 애플릿 속성</title>
     <screenshot>
      <screeninfo>인쇄기 애플릿 속성</screeninfo>
      <Graphic Format="gif" Fileref="./figs/printer-applet-props" srccredit="dcm">
      </graphic>
     </screenshot>
     </figure>
    </sect2>

<!-- ######### DRIVE MOUNT - APPLET ######### -->

    <sect2 id="appletsdrive">
     <title>드라이브 마운트 애플릿</title>
     <para>
     <indexterm>
     <primary>
      애플릿
     </primary>
     <secondary>
      드라이브 마운트
     </secondary>
    </indexterm>
      대부분의 유닉스와 그와 비슷한 시스템은 디스트를 넣은 후에 그 디스크를
      사용하기 위해서는 마운트를 해야만 합니다.
     </para>
     <para>
      드라이브 마운트 애플릿은 당신이나 시스템 관리자가 패널에 있는 아이콘을
      클릭하므로서 마운트를 쉽게 해드립니다. 당신이 마운트를 하기 위해서는
      당신이 접근하고자 하는 드라이브에 대한 접근 권한을 가지고 계셔야 합니다.
     </para>
     <para>
      수퍼유저 권한을 가진 사람이
      <application><emphasis>linuxconf</emphasis></application>을 가지고 있는
      경우에 이 작업은 대단히 쉽습니다. <guilabel>Access local
      drive</guilabel> 섹션에서 접근을 원하는 드라이브를 선택한 후
      <guilabel>Options</guilabel> 탭에서 <guilabel>User
      Mountable</guilabel>을 선택하시면 됩니다. 그러면 모든 사용자가 그
      드라이브를 마운트할 수 있습니다.
     </para>
     <para>
      <application><emphasis>linuxconf</emphasis></application>이 없는
      경우에는 수퍼유저 권한을 가진 사람이 <emphasis>/etc/fstab</emphasis>
      파일을 편집하여 사용자가 마운트 가능하도록 할 수 있습니다. 가령:
     </para>
     <para>
      fstab 파일이 다음과 비슷하다면:
      <programlisting>
       /dev/cdrom /mnt/cdrom iso9660 exec,dev,ro,noauto 0 0
      </programlisting>
      "user"를 네번째 열에 추가합니다:
     <programlisting>
      /dev/cdrom /mnt/cdrom iso9660 user,exec,dev,ro,noauto 0 0
     </programlisting>
     </para>
     <para>
      그러면 수퍼유저가 아니더라도 그 드라이브를 마운트할 수 있습니다.
      <guimenu>애플릿 추가</guimenu> 메뉴에서 <guimenu>도구</guimenu>
      메뉴의 <guimenuitem>드라이브 마운트</guimenuitem>를 선택해서 패널에
      애플릿을 추가할 수 있습니다.
     </para>
     <para>
      그러면 당신은 플로피 드라이브처럼 보이는 작은 드라이브 이미지를 보실 수
      있을 것입니다.
     </para>
     <figure>
      <title>드라이브 마운트 애플릿</title>
      <screenshot>
       <screeninfo>드라이브 마운트 애플릿</screeninfo>
       <Graphic Format="gif" Fileref="./figs/drive" srccredit="dcm">
       </graphic>
      </screenshot>
     </figure>
     <para>
      드라이브 마운트 애플릿은 항상 기본적으로 플로피 드라이브를 사용합니다.
      애플릿에서 오른쪽 마우스를 클릭하여 <guimenu>팝업</guimenu> 메뉴의
      <guimenuitem>속성</guimenuitem>을 선택하여 바꿀 수 있습니다.
     </para>
     <figure>
      <title>드라이브 마운트 애플릿</title>
      <screenshot>
       <screeninfo>드라이브 마운트 애플릿</screeninfo>
       <Graphic Format="gif" Fileref="./figs/drive-prefs" srccredit="dcm">
       </graphic>
      </screenshot>
     </figure>
     <para>
      <guilabel>드라이브 마운트 설정</guilabel> 대화상자에서 마운트할
      드라이브와 그 드라이브의 위치를 설정하실 수 있습니다.
     </para>
     <para>
      첫번째 옵션은 얼마나 자주 갱신할 것인지를 설정하는 것입니다. 갱신하는
      동인에 애플릿은 드라이브가 마운트되어 있는지를 확인하여 적절하게
      애플릿을 표시합니다.
     </para>
     <para>
      두번째 옵션은 어떤 아이콘으로 표시할 것인지를 설정하는 것입니다. 네개의
      아이콘 중에서 하나를 선택할 수 있는 데, 그것은 플로피, CDROM, Zip
      디스크와 하드 드라이브입니다. 아이콘을 선택한 후에는 <guilabel>Mount
      point</guilabel> 텍스트 박스에서 어디에 마운트할 것인지를 적어주셔야
      합니다.
      </para>
      <para>
       마지막 옵션은 <guilabel>Use automount friendly status test</guilabel>를
       사용할 것인지를 결정하는 것입니다. 드라이브를 자동으로 마운트하는
       autofs를 사용할 수 있는 시스템에서는 드라이브 마운트 애플릿과 autofs가
       충돌이 발생할 수 있습니다. 그런 경우에는 이 옵션을 선택하셔야만 합니다.
       autofs(혹은 이것과 유사한)를 사용하지 않으시는 경우에는 이 옵션을
       선택하지 마십시요. 선택하면 시스템이 느려지게 됩니다.
      </para>
    </sect2>

<!-- ######### CLOCK AND MAILCHECK - APPLET ######### 
    <sect2 id="appletsclockmail" >
     <title>Clock and Mailcheck Applet</title>
     <para>

     </para>
    </sect2> -->

<!-- ######## GNOME PAGER - APPLET ######### -->

    <sect2 id="appletspager">
     <title>그놈 페이저</title>
     <para>
      <indexterm>
     <primary>
      애플릿
     </primary>
     <secondary>
      그놈 페이저
     </secondary>
    </indexterm>
    <indexterm>
     <primary>
      페이저
     </primary><see>"애플릿"</see>
    </indexterm>
    <indexterm>
     <primary>
      그놈 페이저
     </primary><see>"애플릿"</see>
    </indexterm>
      그놈 페이저는 모든 가상 데스크탑과 그 안에 있는 응용 프로그램들을
      보여주는 애플릿입니다. 그놈 페이저에는 두개의 주 영역이 있는 데, 그것은
      데스크탑 보기와 응용 프로그램 보기입니다. 데스크탑 보기에서는 각각의
      데스크탑들이 작은 사각형으로 표시됩니다. 거기에 어떤 응용 프로그램이
      있는 경우에는 데스크탑에서의 그들의 위치에 작은 외곽선 표시가 보이게
      됩니다. 응용 프로그램 보기에서는 목록보기에서 활성화된 데스크탑의 응용
      프로그램들이 보이게 됩니다. 화살표를 가진 버튼의 중앙을 누르면 모든
      데스크탑과 그 안의 응용 프로그램들을 한번에 보여드립니다.
     </para>
     <figure>
      <title>그놈 페이저</title>
     <screenshot>
      <screeninfo>그놈 페이저</screeninfo>
      <Graphic Format="gif" Fileref="./figs/gnome-pager" srccredit="dcm">
      </graphic>
     </screenshot>
     </figure>
     <note>
      <title>참고</title>
      <para>
       당신이 아이콘으로 만든 창이 화면에서 사라져버리는 경우에 패널에 그놈
       페이져를 추가하십시요.
      </para>
     </note> 
     <para> 
      페이저에서 오른쪽 마우스를 클릭한 후 <guimenu>팝업</guimenu> 메뉴의
      <guimenuitem>속성</guimenuitem> 메뉴 항목을 선택하여 설정 대화상자를 열
      수 있습니다.
     </para>
     <para>
      그놈 페이저 설정 대화상자는 <guilabel>페이저</guilabel>와
      <guilabel>태스크 목록</guilabel>라는 두개의 탭을 가지고 있습니다.
      <guilabel>페이저</guilabel> 탭은 (데스크탑을 그래픽으로 보여주는)
      페이저의 설정을 바꿀 수 있도록 해 주고, <guilabel>태스크
      목록</guilabel> 탭에서는 (데스크탑에 떠 있는 응용 프로그램 목록인) 태스크
      목록에 대한 설정을 바꿀 수 있습니다.
    </para>
    <figure>
     <title>그놈 페이저 속성</title>
     <screenshot>
      <screeninfo>그놈 페이저 속성</screeninfo>
      <Graphic Format="gif" Fileref="./figs/gnome-pager-props" srccredit="dcm">
      </graphic>
     </screenshot>
    </figure>
    <ITEMIZEDLIST mark="bullet">
     <listitem>
      <para>
       <guilabel>페이저</guilabel> -
      </para>
       <ITEMIZEDLIST mark="bullet">
        <listitem>
         <para>
          <guilabel>페이저 보기</guilabel> - 페이저를 볼 것인지 그렇지
	  않은지를 결정합니다. 페이저를 숨기면 태스크 목록만 보이게 됩니다.
         </para>
        </listitem>
        <listitem>
         <para>
	  <guilabel>페이저를 태스크목록 다음에 위치</guilabel> - 페이저와
	  태스크 목록의 좌우 혹은 위 아래 위치를 바꿉니다.
         </para>
        </listitem>
        <listitem>
         <para>
	  <guilabel>작은 페이저의 넓이</guilabel> - 작은 페이저를 가지고 있는
	  경우 여기에서 넓이를 바꿀 수 있습니다.
         </para>
        </listitem>
        <listitem>
         <para>
	  <guilabel>작은 페이저의 높이</guilabel> - 작은 페이저가 있는 경우 그
	  높이를 바꿀 수 있습니다.
         </para>
        </listitem>
        <listitem>
         <para>
	  <guilabel>큰 페이저의 넓이</guilabel> - 큰 페이저가 있는 경우
	  여기에서 넓이를 바꿀 수 있습니다.
         </para>
        </listitem>
        <listitem>
         <para>
	  <guilabel>큰 페이저의 높이</guilabel> - 큰 페이저가 있는 경우
	  여기에서 그 높이를 바꿀 수 있습니다.
         </para>
        </listitem>
       </itemizedlist>
      </listitem>
     </itemizedlist>
     <ITEMIZEDLIST mark="bullet">
      <listitem>
       <para>
        <guilabel>태스크 목록</guilabel> - 
       </para>
       <ITEMIZEDLIST mark="bullet">
        <listitem>
         <para>
          <guilabel>태스크 목록 버튼 보이기</guilabel> - 태스크 목록을 볼
	  것인지를 결정합니다. 태스크 목록 버튼은 눌려졌을 때 모든 데스크탑의
	  모든 응용 프로그램을 보여줄 수 있는 위 화살표를 가진 작은
	  버튼입니다.
         </para>
        </listitem>
        <listitem>
         <para>
	  <guilabel>태스크 목록 보기</guilabel> - 태스크 목록을 볼 수 있고,
	  이걸 선택하지 않으면 페이저만 보이게 됩니다.
         </para>
        </listitem>
        <listitem>
         <para>
	  <guilabel>버튼 아이콘 보기</guilabel> - 태스크 목록 버튼에서
	  아이콘을 볼 것인지를 결정합니다. 이걸 끄면 약간의 공간을 절약할 수
	  있습니다.
         </para>
        </listitem>
        <listitem>
         <para>
	  <guilabel>보일 태스크</guilabel> - 어떤 응용 프로그램이 태스크
	  목록에 보이도록 할 것인지, 그리고 언제 보일 것인지를 결정합니다.
	  모든 태스크, 일반 태스크, 아이콘화한 태스크, 모든 데스크탑의 태스크,
	  혹은 모든 데스크탑의 아이콘화한 태스크 등을 선택할 수 있습니다.
         </para>
        </listitem>
        <listitem>
         <para>
	  <guilabel>Geometry</guilabel> - 태스크 목록의 크기등을 바꿀 수
	  있습니다. 태스크 목록이 항상 가장 작은 크기로 보이도록 할 수도 있고,
	  수평과 수직의 태스크 목록의 넓이를 바꿀 수도 있습니다. 태스크 목록의
	  열과 행의 수를 바꿀 수도 있습니다.
         </para>
        </listitem>
       </itemizedlist>
      </listitem>
     </itemizedlist>
    </sect2>


<!-- ######### ESOUND MANAGER - APPLET ######### 

    <sect2 id="appletsesd">
     <title>Esound Manager Applet</title>
     <para>
      TO BE DONE...
     </para>
    </sect2> -->

<!--######### QUICKLAUNCH - APPLET ######### -->

<!-- 현재 version에는 없음 - 1999/08/23 namsh
    <sect2 id="quicklaunch">
     <title>Quicklaunch Applet</title>
     <para>
     <indexterm>
     <primary>
      애플릿
     </primary>
     <secondary>
      Quicklaunch
     </secondary>
    </indexterm>
      The Quicklaunch Applet is a small applet that gives you a
      repository to place launchers. The Quicklaunch applet holds the
      application launchers you wish to have and allows you to click
      on them to launch the applications. Drag and drop functionality
      makes the set up of the Quicklaunch applet very easy and
      quick.
     </para>
     <figure>
      <title>The Quicklaunch Applet</title>
      <screenshot>
       <screeninfo>The Quicklaunch Applet</screeninfo>
       <Graphic Format="gif" Fileref="./figs/quicklaunch" srccredit="dcm">
       </graphic>
      </screenshot>
     </figure>
     <para>
      To add a launcher to the Quicklaunch applet you must already have the
      launcher set up either in the Main Menu, on your Panel, or on your
      desktop. Once you have the launcher you may drag it onto the Quicklaunch
      applet to create a small launcher button. To launch the application simply
      press the launcher button.
     </para>
     <para>
      To drag a launcher from your Main Menu click once on the Main Menu to open
      it, and click once on the sub menu to open it. Now move the mouse to the
      application desired, press (but do not release) the left mouse button,
      drag the application to the Quicklaunch applet and release the button.  To
      drag a launcher from either the Panel or your desktop, move the mouse to
      the launcher, press and hold the left mouse button, drag the application
      to the Quicklaunch applet, and release the button.
     </para>
     <para>
      Once the launcher is in the Quicklaunch applet you may right mouse click
      on it and select <guilabel>Properties</guilabel> to change any properties
      associated with the launcher. The dialog that is launched is the standard
      GNOME launcher properties dialog, which you can read more about in <xref
      linkend="panelapplaunch">.
     </para>
    </sect2>
-->

   </sect1>
 </chapter>

<!-- ############# GNOME CD PLAYER - APPLICATION ############# -->

<chapter id="gtcd">
<title>그놈 CD 연주기</title>
 <sect1>
  <title>소개</title>
  <para>
  <indexterm>
   <primary>
    그놈 CD 연주기
   </primary>
  </indexterm>
   그놈 CD 연주기(gtcd)는 그놈이 뜰때 미리 실행되는 그놈화한 응용
   프로그램입니다. 단순한 CD 연주기로서 컴팩트 디스크의 노래를 들을 수 있게 해
   줍니다.
  </para>
 </sect1>
 <sect1 id="gtcd-use">
  <title>그놈 CD 연주기 사용</title>
   <para>
    그놈 CD 연주기는 주 메뉴의 오디오 메뉴에서 찾을 수 있고, 명령행에서
    <command>$ gtcd</command>를 입력해서 실행할 수도 있습니다.
   </para>
   <figure>
    <title>그놈 CD 연주기</title>
   <screenshot>
    <screeninfo>그놈 CD 연주기</screeninfo>
    <Graphic Format="gif" Fileref="./figs/gtcd" srccredit="dcm">
    </graphic>
   </screenshot>
   </figure>
   <important>
    <title>중요</title>
    <para>
      이 응용프로그램이 제대로 동작하기 위해서는 당신이 CDROM 드라이브에 대한
      접근권한을 가지고 있어야 합니다. 어떤 시스템은 당신이 콘솔로 로그인하는
      순간 자동으로 그러한 권한을 당신에게 부여할 지도 모릅니다. 이런
      메카니즘을 <application>pam_console</application>이라고 합니다. 시스템이
      권한을 부여하지 않은 경우에는 그런 권한을 받을 필요가 있습니다. 루트
      비밀번호를 알고 계신다면 터미널에서 다음과 같이 하십시요.
     <programlisting>
      $ su 
      $ Password: [루트 비밀번호 입력] 
      $ chmod a+r /dev/cdrom
      $ exit
     </programlisting>
    </para>
    <para>
     CDROM이 /dev/cdrom이 아닌 경우에는 위의 명령을 바꿀 필요가 있습니다.
    </para>
   </important>
   <para>
    그놈 CD 연주기는 연주, 중지, 멈춤등의 버튼을 지원하여 다른 연주기들과 같이
    동작합니다. 더불어 드롭다운 메뉴에서 트랙 제목을 표시해 주는 트랙 선택기가
    있습니다. <guibutton>속성</guibutton> 버튼을 눌러서 여러가지 속성을 바꿀
    수도 있습니다. 이 버튼을 누르면 그놈 CD 연주기 속성 대화상자가 열립니다.
   </para>
   <para>
    그놈 CD 연주기 속성 대화상자에는 세개의 탭이 있습니다: 속성, 글쇠결합,
    그리고 CDDB 설정이 그것입니다.
   </para>
   <figure>
    <title>그놈 CD 연주기 속성</title>
   <screenshot>
    <screeninfo>그놈 CD 연주기 속성</screeninfo>
    <Graphic Format="gif" Fileref="./figs/gtcd-props" srccredit="dcm">
    </graphic>
   </screenshot>
   </figure>
   <ITEMIZEDLIST MARK="bullet">
    <listitem>
     <para>
       속성 탭 - 여기에서 설정가능한 것들:
     </para>     
     <para> 
      그놈 CD 연주기가 처음 시작될 때와 종료시 하고 싶은 것.
     </para>
     <para> 
      CDROM의 위치. 일반적으로는 <command>/dev/cdrom</command>입니다.
     </para>
     <para>
      표시되는 트랙과 CD 제목의 색상.
     </para>
     <para> 
      표시되는 트랙과 CD 제목의 글꼴.
     </para>
     <para>
      CD 연주기에서 제목 창을 끌어다 데스크탑에 위치시킬 수 있는 기능.
     </para>
     <para> 
      마우스가 버튼 위에 있을 때 툴팁을 보여주는 기능.
     </para>
    </listitem>
    <listitem>
     <para>
      글쇠결합 탭 - 이 탭에서는 그놈 CD 연주기에서 사용되는 글쇠결합을 바꿀 수
      있습니다. 이러한 글쇠결합은 마우스없이도 연주기를 사용할 수 있도록 해
      드립니다. 글쇠결합을 바꾸고 싶으시면 마우스로 선택하여 <command>바꾸려면
      여기를 누르세요</command> 텍스트 상자에서 새로운 글쇠를 누르십시요.
     </para>
    </listitem>
    <listitem>
     <para>
      CDDB 설정 탭 - CDDB는 CD 데이타베이스의 약어이며 CD 정보에 대한 엄청난
      양의 데이타베이스를 가지고 있습니다. 각각의 CD는 CD 연주기가 읽을 수
      있는 신분증같은 걸 가지고 있습니다. 인터넷에 연결되어 있다면 그
      신분증으로 CDDB 서버에서 자료를 얻어올 수 있습니다. 얻어올 수 있는
      자료는 보통 CD 제목, 아티스트와 트랙 제목등입니다. 자료가 한번 얻어지면
      그놈 CD 연주기는 나중을 위해 하드 디스크에 해당 자료를 저장해 놓습니다.
      CDDB 설정 탭에서는 CDDB 서버를 바꾸거나 로컬 CDDB 데이타베이스를 편집할
      수 있습니다. CDDB에 대해서는 <ulink URL="http://www.cddb.org"
      TYPE="http">CDDB 웹사이트</ulink>를 방문하시면 더 많은 정보를 얻으실 수
      있습니다.
     </para>
    </listitem>
   </itemizedlist>
   <para>
    그놈 CD 연주기는 트랙 편집 기능도 가지고 있습니다. 트랙 편집기는 그놈 CD
    연주기 창에서 <guibutton>트랙 편집기</guibutton> 버튼을 눌러서 열 수
    있습니다. 트랙 편집기는 CD에 대한 CDDB 정보가 없거나 잘못된 경우에 CD 트랙
    정보를 바꿀 수 있게 해 드립니다. 트랙 편집기의 바닥에 있는 <guibutton>CDDB
    상태</guibutton> 버튼을 눌러서 CDDB 정보의 상태를 확인해 보실 수도
    있습니다. 이 경우 어떤 경우가 되었든지 CDDB 서버에서 얻어진 정보가 보이게
    될 것입니다.
   </para>
   <figure>
    <title>그놈 CD 연주기 트랙 편집기</title>
   <screenshot>
    <screeninfo>그놈 CD 연주기 트랙 편집기</screeninfo>
    <Graphic Format="gif" Fileref="./figs/trackeditor" srccredit="dcm">
    </graphic>
   </screenshot>
   </figure>
  </sect1>
 </chapter>

<!-- ############# GNOME CALENDAR - APPLICATION ############# -->

 <chapter id="gcal">
  <title>그놈 달력</title>
  <sect1>
   <title>소개</title>
   <para>
   <indexterm>
    <primary>
     그놈 달력
    </primary>
   </indexterm>
   <indexterm>
    <primary>
     달력
    </primary>
   </indexterm>
    그놈 달력은 매일 진행되는 작업에 유용하게 사용될 수 있는 달력
    프로그램입니다. 달력은 유용한 프로그램이지만 그놈처럼 아직 초기 개발단계에
    있기 때문에 네트워크 공유나 팜 파일롯같은 PDA와의 동기 맞추기등 추가되어야
    할 기능이 더 있습니다. 달력은 주 메뉴의 <guimenu>응용 프로그램</guimenu>
    하위 메뉴에서 실행시킬 수 있습니다.
   </para>
   <para>
    달력은 날짜별 보기, 주별 보기, 월별 보기, 년별 보기등의 네개의 탭이
    있습니다. 각각의 보기는 각 시간 주기로 보여주며 어느 탭에서든지 스케쥴
    관리를 할 수 있도록 해 드립니다.
   </para>
   <figure>
     <title>그놈 달력</title>
    <screenshot>
     <screeninfo>그놈 달력</screeninfo>
      <Graphic Format="gif" Fileref="./figs/calday" srccredit="dcm">
      </graphic>
    </screenshot>
    </figure>
  </sect1>
  <sect1 id="calsetup">
   <title>그놈 달력 설정</title>
   <para>
     그놈 달력은 처음 깔린대로 사용될 수도 있지만 당신이 원하는 대로 설정을
     바꿀 수도 있습니다. 먼저 그놈 달력 속성을 바꾸십시요.
     <guimenu>설정</guimenu> 메뉴의 <guimenuitem>속성</guimenuitem> 메뉴
     항목을 선택해서 선택사항 대화상자를 열 수 있습니다.
   <figure>
    <title>그놈 달력 선택사항</title>
   <screenshot>
    <screeninfo>그놈 달력 선택사항</screeninfo>
    <Graphic Format="gif" Fileref="./figs/cal-props" srccredit="dcm">
    </graphic>
   </screenshot>
   </figure>
   </para>
   <para>
    달력 선택사항 대화상자는 시간 표시, 색상, 할일 목록이라는 세개의 탭을
    가지고 있습니다.
   </para>
   <para>
    시간 - 이 탭은 시간 형식, 한주가 시작되는 요일, 하루의 범위라는 세개의
    탭을 가지고 있습니다.
   </para>
   <para>
      시간 형식 - 12 혹은 24 시간 형식을 선택할 수 있습니다.
   </para>
   <para>
     한주가 시작되는 요일 - 일요일부터 시작할 것인 지 아니면 월요일부터 시작할
     것인지를 선택하실 수 있습니다. 이것은 날짜별 혹은 주별 보기의 화면 구성에
     영향을 줍니다.
   </para>
   <para>
     하루의 범위 - 하루의 시작 시간과 끝 시간을 선택할 수 있습니다. 이 범위
     밖의 시간은 어둡게 보일 것입니다.
   </para>
   <para>
    색상 - 이 탭에서는 달력에서 사용되는 기본적인 색을 바꿀 수 있습니다. 8개의
    색이 사용되는 데, 테두리, 머리말, 비어있는 날짜, 약속, 강조된 날짜, 날짜에
    쓸 숫자, 현재 날짜의 숫자, 기한이 지난 할일 항목등입니다. 각각은 작은 색
    선택 상자를 오른쪽에 가지고 있습니다. 그 상자를 누르시면 원하는 색을
    선택할 수 있도록 색 선택 대화상자가 열립니다. 색을 선택하면 오른쪽의
    미리보기에서 확인하실 수 있습니다.
   </para>
   <para>
    할일 목록 -  이 탭에서는 날짜별 보기의 할일 목록에 보일 열을 선택하실 수
    있습니다. 만기일과 우선순위 열을 선택하실 수 있습니다.
   </para>
   <para>
    달력 선택사항에서 내용이 바뀌었다면 <guibutton>적용</guibutton> 버튼을
    눌러서 바뀐 내용을 적용할 수 있습니다.
   </para>
  </sect1>
  <sect1 id="caluse">
   <title>그놈 달력 사용</title>
   <para>
    그놈 달력의 사용은 간단하며 대부분의 작업을 날짜별, 주별, 월별, 년별
    보기에서 할 수 있습니다. 기억해야할 중요한 기능은 언제든지 특정한 날짜를
    오른쪽 마우스로 클릭하여 새로운 약속을 추가할 수 있다는 것입니다. 그
    외에도 많은 기능들이 있으며 밑에서 설명됩니다.
   </para>
     <sect2 id="calday">
    <title>날짜별 보기</title>
    <para>
     날짜별 보기는 그놈 달력에서 가장 유용한 것일 겁니다. 탭의 왼쪽에는 오늘의
     시간이 보입니다. 밝은 회색은 작업 시간과 그렇지 않은 시간을 구분합니다.
     작업 시간을 바꾸고 싶으시면 <xref linkend="calsetup">에서 그렇게 할 수
     있습니다.
    </para>
    <figure>
     <title>날짜별 보기</title>
      <screenshot>
       <screeninfo>날짜별 보기</screeninfo>
       <Graphic Format="gif" Fileref="./figs/calday" srccredit="dcm">
       </graphic>
      </screenshot>
    </figure>
    <tip>
     <title>팁</title>
     <para>
      날짜별 보기에서 약속을 추가할 수 있는 팁 하나를 알려드리면 시간 목록에서
      특정 시간을 클릭한 후 끌어서 선택한 후 Enter를 치시고 약속을 입력하시는
      겁니다. 이렇게 해서 새로운 약속 생성 대화상자를 열지 않고도 약속을
      입력할 수 있습니다.
     </para>
    </tip> 
    <para>
     시간 목록의 오른쪽 위에는 작은 달력이 있습니다. 거기에서 위의 좌우
     화살표를 이용하여 달이나 년도를 바꿀 수 있습니다. 그 작은 달력에서 특정한
     날짜를 더블클릭하면 왼쪽의 시간 목록이 그 날짜로 바뀝니다.
    </para>
    <para>
     작은 달력의 밑에는 할일 목록이 있습니다. 할일 목록은 당신이 해야 할 일을
     한 눈에 볼 수 있도록 해 주는 간단한 목록입니다. 할일 목록에 항목을
     추가하려면 <guibutton>추가</guibutton> 버튼을 누르십시요. 그러면 항목을
     입력할 수 있는 작은 편집 상자가 열립니다. 할일 목록에 항목을 추가한
     후에는 <guibutton>편집</guibutton>과 <guibutton>삭제</guibutton> 버튼을
     이용하여 항목들을 관리할 수 있습니다. 할일 항목들은 날짜별 보기에 어떤
     날이 표시되든지에 상관없이 항상 표시되며 <guibutton>삭제</guibutton>
     버튼을 이용해서만 지울 수 있습니다.
    </para>
   </sect2>
   <sect2 id="calweek">
    <title>주별 보기</title>
    <para>
     주별 보기는 현재 주를 자세한 약속 내용과 함께 보여줍니다. 주별 보기의
     어떤 날에 약속을 추가하고 싶으시면 그 날짜 위에서 오른쪽 마우스를 클릭한
     후 <guimenu>팝업</guimenu> 메뉴에서 <guimenuitem>새 약속</guimenuitem> 
     메뉴 항목을 선택하시면 됩니다. 또한 특정한 날짜를 더블클릭함으로서 날짜별
     보기로 가서 해당 날짜의 자세한 내용을 볼 수도 있습니다.
    </para>
    <figure>
     <title>주별 보기</title>
      <screenshot>
       <screeninfo>주별 보기</screeninfo>
       <Graphic Format="gif" Fileref="./figs/calweek" srccredit="dcm">
       </graphic>
      </screenshot>
    </figure>
    <para>
     주별 보기의 밑 왼쪽 구석에 작은 달력이 있습니다. 거기에서 좌우 화살표를
     눌러서 달 혹은 년도를 바꿀 수 있습니다. 또한 특정한 날을 더블클릭하면
     주별 보기가 그 날짜를 포함하는 주로 바뀝니다.
    </para>
   </sect2>
   <sect2 id="calmonth">
    <title>월별 보기</title>
    <para>
     월별 보기는 간단한 약속 내용과 함께 현재 달 전체를 보여줍니다. 월별
     보기는 사용자가 설정한 색을 사용합니다. 색을 설정하는 방법은 <xref
     linkend="calsetup">을 읽어보시기 바랍니다.  간단한 약속 내용이 있는 날을
     클릭하면 팝업 창에 자세한 약속 내용을 보여줍니다. 약속을 추가하고 싶으면
     날짜를 오른쪽 마우스로 클릭한 후 <guimenu>팝업</guimenu> 메뉴에서
     <guimenuitem>오늘의 새 약속</guimenuitem>을 선택하시면 됩니다.
     <guimenu>팝업</guimenu> 메뉴의 항목들에서 <guimenuitem>오늘로
     점프</guimenuitem>나 <guimenuitem>이번주로 점프</guimenuitem> 혹은
     <guimenuitem>올해로 점프</guimenuitem>를 선택하여 날짜별, 주별, 년별
     보기로 바꿀 수 있습니다.
    </para>
    <figure>
     <title>월별 보기</title>
      <screenshot>
       <screeninfo>월별 보기</screeninfo>
       <Graphic Format="gif" Fileref="./figs/calmonth" srccredit="dcm">
       </graphic>
      </screenshot>
    </figure>
   </sect2>
   <sect2 id="calyear">
    <title>년별 보기</title>
    <para>
     년별 보기는 한 해를 전부 보여주지만 약속 내용은 보여주지 못합니다. 월별
     보기처럼 년별 보기도 사용자가 설정한 색을 사용합니다. 색을 설정하는
     방법은 <xref linkend="calsetup">에서 읽어보실 수 있습니다. 어떤 날에
     약속이 있는 경우 그 날짜를 클릭하면 팝업 창에서 약속의 내용을 보실 수
     있습니다. 약속을 추가하려면 해당 날짜를 오른쪽 마우스로 클릭한 후에
     <guimenuitem>오늘의 새 약속</guimenuitem>을 <guimenu>팝업</guimenu>
     메뉴에서 선택하시면 됩니다. <guimenu>팝업</guimenu> 메뉴의 항목들에서
     <guimenuitem>오늘로 점프</guimenuitem>나 <guimenuitem>이번주로
     점프</guimenuitem> 혹은 <guimenuitem>이번 달로 점프</guimenuitem>를
     선택하여 날짜별, 주별, 월별 보기로 바꿀 수 있습니다.
    </para>
    <figure>
     <title>년별 보기</title>
      <screenshot>
       <screeninfo>년별 보기</screeninfo>
       <Graphic Format="gif" Fileref="./figs/calyear" srccredit="dcm">
       </graphic>
      </screenshot>
    </figure>
   </sect2>
  </sect1>
  <sect1 id="calappoint">
    <title>새 약속 만들기</title>
    <para>
     그놈 달력에서 새 약속을 만드는 방법은 많습니다만 가장 쉬운 방법은 버튼
     막대에서 <guibutton>새 것</guibutton>을 선택하는 것입니다. 새 약속을
     만들려면 새 약속 만들기 대화상자를 열어서 약속에 대한 여러가지 설정을
     해야 합니다. 새 약속 만들기 대화상자는 일반과 반복이라는 두개의 탭을
     가지고 있습니다.
    </para>
    <para>
     일반 - 일반 탭에서는 약속 시간과 알람 등을 설정할 수 있습니다. 거기에는
     요약, 시각, 알람, 분류라는 네가지의 다른 영역이 있습니다.
     <ITEMIZEDLIST MARK="bullet">
      <listitem>
       <para>
        요약 - 요약 상자에서는 약속에 대한 설명을 입력할 수 있습니다.
	이 부분은 주별 보기와 월별 보기에서만 보인다는 사실을 명심하십시요.
       </para>
      </listitem>
      <listitem>
       <para>
        시각 - 시각 영역에서는 약속의 날짜 및 시간 구간을 설정할 수 있습니다.
	시작 시각과 끝 시각의 오른쪽에 달력이라고 이름붙여진 선택상자가
	있습니다. 그것을 누르면 작은 달력이 보이는 데, 거기에서 시작과 끝
	날짜를 선택할 수 있습니다. 시간 목록의 각각의 시간은 15분 단위로
	분리된 시간을 하위 메뉴로 가지고 있으므로 이런 시간 단위로 선택을 하실
	수도 있습니다. 시간은 선택된 날짜 안의 범위로 제한됩니다.
       </para>
      </listitem>
      <listitem>
       <para>
        알람 - 알람은 당신이 약속을 기억할 수 있도록 알람을 설정하도록 해
	줍니다. 표시, 오디오, 프로그램, 전자우편이라는 네가지 종류의 알람을
	사용하실 수 있습니다. 표시 알람은 당신이 설정한 시간에 화면에 메시지를
	표시해 주는 것입니다. 오디오 알람은 당신이 설정한 시간에 오디오 파일을
	연주합니다. 프로그램 알람은 당신이 설정한 시간에 해당 프로그램을
	실행합니다. 전자우편 알람은 당신이 설정한 시간에 해당 사용자에게
	편지를 보냅니다.
       </para>
      </listitem>
     <!-- <listitem>
       <para>
        Classification - TO BE DONE
       </para>
      </listitem> -->
     </itemizedlist>
    </para>
    <para>
     반복 - 반복 탭은 약속이 반복되어야 하는 경우에 사용될 수 있습니다.
     첫번째로 설정해야 할 것은 반복 규칙입니다. 없음, 매일, 매주, 매달, 매년
     중 하나를 선택하셔야 합니다. 각각의 선택에 대해 반복 속성을 조정할 수
     있습니다. 끝나는 날에서 약속이 끝나는 날을 설정하거나 영원히 반복되도록
     할 수 있습니다. 예외 영역에서 날짜를 더블클릭한 후
     <guibutton>예외 추가</guibutton> 버튼을 눌러서 예외를 둘 수 있습니다.
    </para>
   </sect1>
 </chapter>
