<!-- ################### ABOUT WINDOW MANAGERS ###################### -->

 <chapter id="wm">
  <title>Window Managers and GNOME</title>
   <sect1 id="wmabout">
    <title>About Window Managers</title>
    <para>
      In Unix and Linux, nearly all graphical applications use a lower-level
      software component called the "X window system" for managing the mouse,
      keyboard, and low-level display issues.  A separate piece of software,
      called a "window manager," works with the X window system to control
      windows.
    </para>
    <para>
     The window manager is the piece of software that controls the windows in
     the X window environment. The placement, borders, and decorations of any
     window are managed by the window manager. This is very different from many
     other operating systems, and the way GNOME deals with window managers is
     different from other desktop environments.
    </para>
    <para>
     GNOME is not dependent on any one window manager. This means that major
     parts of your desktop environment will not change when you decide to switch
     window managers. GNOME works with the window manager to give you the
     easiest work environment you can have. GNOME does not worry about window
     placement but gets information from the window manager about their
     placement.  However, some GNOME features require a "GNOME compliant window
     manager" to work correctly, in particular the Desk Guide applet as well as
     drag and drop on the desktop.  In most cases, we'd recommend the user of a
     GNOME compliant window manager for new users.
    </para>
    <para>
      At the time of this writing, there are only two window managers 100%
      GNOME-compliant, Sawmill and Enlightenment.  There are many other window
      managers that are partially compliant or are being modified to meet
      compliance.
    </para>
    <para>
     Some of the window managers that have partial to full compliance at the
     time of this version of the GNOME User's Guide are:
    </para>
    <itemizedlist mark="bullet">
     <listitem>
      <para>
       <ulink url="http://sawmill.sourceforge.net"
       type="http">Sawfish</ulink> (formerly named Sawmill)
      </para>
     </listitem>
     <listitem>
      <para>
       <ulink url="http://icewm.sourceforge.net"
       type="http">IceWM</ulink>
      </para>
     </listitem>
     <listitem>
      <para>
       <ulink url="http://www.windowmaker.org/"
       type="http">WindowMaker</ulink>
      </para>
     </listitem>
     <listitem>
      <para> 
       <ulink url="http://www.enlightenment.org/"
       type="http">Enlightenment</ulink>
      </para>
     </listitem>
     <listitem>
      <para>
       <ulink url="http://www.afterstep.org/"
       type="http">AfterStep</ulink>
      </para>
     </listitem>
     <listitem>
      <para>
       <ulink url="http://www.fvwm.org/"
       type="http">FVWM2</ulink>
      </para>
     </listitem>
    </itemizedlist>
    <para>
     There are a host of newer window managers being developed that will work
     with GNOME. You can find some of these on the <ulink
     url="http://www.gnome.org/applist/">GNOME Software Map</ulink>.
    </para>
   </sect1>
   <sect1 id="wmchange">
    <title>Changing Window Managers</title>
     <para>
      At any time you may change the window manager you are using by utilizing
      the Window Manager Capplet in the GNOME Control Center. You may read more
      about this Capplet in <xref linkend="gccwm ">
     </para>
     <IMPORTANT>
      <title>IMPORTANT</title>
      <para>
       Keep in mind that the window manager you choose to use may not be
       compliant with GNOME and you may not benefit from some of the GNOME
       features if you use it.
      </para>
     </important>
   </sect1>
 </chapter>
