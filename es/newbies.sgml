<appendix id="newbies" label="A"> 
  <title>Si usted es nuevo en Linux/Unix</title>
  
  <para> Uno de los objetivos de Gnome es hacer su sistema f�cil de usar,
     sin que usted necesite aprender la sintaxis de la mayor�a de los
     comandos Unix. No obstante, hay algunas nociones b�sicas de Unix
     con las que debe familiarizarse, incluso cuando utilice la interfaz
     gr�fica amigable suministrada por Gnome. Para los nuevos usuarios,
     estos comandos se engloban en este ap�ndice. Si usted necesita m�s
     informaci�n acerca de UNIX, debe leer la documentaci�n que vino con
     su sistema, existen tambi�n numerosos libros y gu�as Internet que
     est�n disponibles para todas las versiones de UNIX.
  </para>

  <para> La siguiente gu�a se aplica a todas las versiones de UNIX y todos 
     los sistemas operativos parecidos a UNIX , incluidos ambos: comerciales
     como <systemitem>Solaris</systemitem> y sistemas operativos de c�digo
     abierto como <systemitem>BSD</systemitem> y <systemitem>Linux</systemitem>.
     Parte del material de esta gu�a est� basado en la gu�a
     <citetitle>Instalaci�n de Linux e Inicio</citetitle>, escrito por 
     Matt Welsh, Phil Hughes, David Bandel, Boris Beletsky, Sean Dreilinger, 
     Robert Kiesling, Evan Liebovitch y Henry Pierce. La gu�a est� disponible 
     para descargar o consultar directamente en la direcci�n URL
     <ulink url="http://www.linuxdoc.org" type="http">Proyecto de
     Documentaci�n Linux</ulink> o del <ulink url="http://www.oswg.org">
     Grupo de Escritores de C�digo Abierto</ulink>.
  </para>
  <sect1 id="new-users">
    <title>Usuarios</title> 
    <para> UNIX es un sistema operativo multiusuario: fue designado para
      permitir a varios usuarios trabajar en la misma computadora, ya sea 
      simult�neamente (utilizando varios terminales o en conexiones de red)
      o en turnos. En sistemas UNIX, para identificarse usted en el sistema,
      debe conectarse, lo que conlleva introducir su <emphasis>nombre de
      acceso</emphasis> (el nombre que el sistema utiliza para 
      identificarle) y su <emphasis>contrase�a</emphasis>, que es su clave
      personal para introducirse en su cuenta personal. Porque s�lo usted
      conoce su clave de acceso, nadie m�s puede introducirse en el sistema
      con su nombre de acceso. Normalmente se escoge el nombre, el apellido
      o alguna variaci�n de �stos como su nombre de acceso, por lo que si
      su nombre es: Sasha Beilinson, su nombre de acceso podr�a ser:
      <systemitem>Sasha</systemitem>.
    </para>
    <para> Cada usuario tiene un espacio separado para guardar sus
      documentos (llamado <emphasis>directorio personal</emphasis>).
      UNIX posee un sistema de permisos (ver <xref linkend="permissions">),
      por lo cual en un Sistema UNIX apropiadamente configurado, un usuario
      no puede cambiar archivos pertenecientes a otro usuario o al sistema.
      Esto tambi�n permite a cada usuario, configurar varios aspectos del
      sistema &mdash;  para s� mismo, sin afectar a otros.
    </para>
    <para> En cada sistema UNIX, hay un usuario especial, llamado
      <emphasis>administrador del sistema</emphasis>, con el nombre de
      acceso <systemitem>root</systemitem>. Este usuario tiene
      <emphasis>completo</emphasis> control sobre el sistema &mdash;
      incluyendo acceso total a todos los archivos de sistema y de los
      usuarios. Tiene autoridad para cambiar las claves de acceso de los
      usuarios existentes y a�adir nuevos usuarios, instalar y desinstalar
      software, adem�s de otras cosas. El administrador del Sistema, es
      normalmente la persona responsable del correcto funcionamiento
      del sistema, por lo que si presenta alg�n problema, debe acudir a �l.
    </para>
    <important>
      <title>IMPORTANTE</title>
      <para> Incluso si usted es el �nico usuario en su ordenador (por
        ejemplo, si es ordenador personal), por lo que usted tambi�n es el
        administrador del sistema, es importante que cree una cuenta de
        usuario y la utilice para su trabajo diario, accediendo como
        administrador ("root") s�lo cuando sea necesario para el
        mantenimiento del sistema. Dado que "root" puede hacer cualquier
        cosa, es f�cil cometer errores que tengan consecuencias catastr�ficas.
        Imagine el usuario "root" como un sombrero m�gico que le da a usted
        mucho poder, con el que puede, con un movimiento de manos, crear o
        destruir ciudades enteras. Ya que es f�cil mover las manos de forma
        destructiva, no es una buena idea utilizar el sombrero m�gico
        cuando no es necesario, a pesar de lo maravilloso que se siente.
      </para>
    </important>
  </sect1>
  <sect1 id="new-file">
    <title>Archivo y nombres de archivos</title>
    <para> En la mayor�a de los sistemas operativos (incluyendo UNIX),
      existe el concepto de <emphasis>archivo</emphasis>, que es s�lo
      un conjunto de informaci�n con un nombre (llamado <emphasis>nombre
      del archivo</emphasis>. Ejemplos de archivos, podr�an ser su examen
      final de historia, un mensaje de correo electr�nico, o un programa
      que pueda ser ejecutado. Esencialmente, cualquier cosa guardada
      en disco es guardado en un archivo individual.
    </para>
    <sect2 id="new-filenames">
      <title>Nombres de archivos</title>
      <para>
        Los archivos son identificados por sus nombres. Por ejemplo, el
        archivo que contiene una conferencia hablada, puede ser guardada
        bajo el nombre de archivo <filename>talk.txt</filename>. No existe
        un formato est�ndar para los nombres de los archivos, como existe
        en MS-DOS y otros sistemas operativos; en general, el nombre de un
        archivo puede contener cualquier car�cter (excepto el car�cter &ndash;
        vea la explicaci�n de los caminos de nombres debajo) y es limitado
        ,en su extensi�n, a 256 caracteres.
      </para>
      <important>
	<title>IMPORTANTE</title>
	<para>
          A diferencia de MS-DOS, los nombres de archivo en UNIX son sensibles
          a may�sculas o min�sculas: <filename>midocumento.txt</filename>
          y <filename>MiDocumento.txt</filename>, son considerados como
          dos archivos diferentes.
	</para>
      </important>
      <para>
        Tambi�n debe conocer algunas convenciones en UNIX, que si bien no
        son obligatorias, normalmente es una buena idea seguirlas.
	<itemizedlist>
	  <listitem>
	    <para> Es costumbre utilizar el formato
              <filename>nombrearchivo.extensi�n</filename>, para nombres
              de archivo, en el que la extensi�n indica el tipo de archivo;
              por ejemplo, la extensi�n <filename>txt</filename> es
              normalmente utilizada para archivos de texto simple;
              en tanto que la extensi�n jpeg es utilizada para gr�ficos en
              formato JPEG, y as�. En particular, la aplicaci�n
              <application>Gestor de Archivos de Gnome</application>
              (<application>GMC</application>) utiliza extensiones para
              determinar el tipo de archivo. Usted puede ver todas las
              extensiones de archivo reconocidas por
              <application>GMC</application>, escogiendo la opci�n
              <guimenuitem>Editar tipos MIME</guimenuitem>  en el men�
              <guimenu>Comandos</guimenu> de <application>GMC</application>.
              Note que la convenci�n est�ndar en UNIX es que los 
              <emphasis>ejecutables</emphasis> no tienen extensiones.
            </para>
          </listitem>
	  
	  <listitem>
	    <para> Los archivos y directorios cuyos nombres comienzan con
              un punto (.), son normalmente, <emphasis>archivos de
              configuraci�n</emphasis>, esto significa que estos archivos
              contienen propiedades y preferencias para varias aplicaciones.
              Por ejemplo, Gnome guarda todos sus configuraciones en varios
              archivos en los directorios <filename>.gnome</filename> y
              <filename>.gnome-desktop</filename> en el directorio personal
              del usuario. Como la mayor parte del tiempo usted no necesita
              editar estos documentos manualmente, ni siquiera conocer su
              preciso nombre o localizaci�n, <application>GMC</application>
              no suele mostrar estos archivos. Usted puede cambiar la
              configuraci�n utilizando el di�logo
              <guimenuitem>Preferencias</guimenuitem> (ver <xref
              linkend="gmcprefs">).
	    </para>
	  </listitem>
	  
	  <listitem> 
	    <para> Los archivos cuyos nombres terminan con (~), por lo
              general son archivos de soporte (copias de seguridad) creados
              por varias aplicaciones. Por ejemplo, cuando usted edita
              un archivo <filename>miarchivo.txt</filename> con
              <application>emacs</application>, se guarda la versi�n previa
              en el archivo <filename>miarchivo.txt~</filename>. Otra vez,
              usted puede controlar si quiere que el Gestor de Archivos
              GNOME le muestre estos archivos o no en el di�logo
              <guimenuitem>Preferencias</guimenuitem> (ver <xref
              linkend= "gmcprefs">).
	    </para>
	  </listitem>
	</itemizedlist>
      </para>
    </sect2>
    <sect2 id="new-wildcards">
      <title> Caracteres comod�n</title> 

      <para> Cuando usted entra �rdenes desde la l�nea de comandos,
        puede utilizar los llamados <emphasis>Caracteres comod�n</emphasis>,
        en lugar de un nombre de archivo exacto. El car�cter comod�n m�s
        com�n es *, que corresponde a cualquier secuencia de s�mbolos
        (incluyendo una l�nea vac�a). Por ejemplo, la orden
        <command>ls *.txt</command> va a listar todos los archivos con la
        extensi�n <filename>txt</filename>, y la orden 
        <command>rm cap�tulo*</command> va a borrar todos los archivos cuyos
        nombres comiencen con  <filename>cap�tulo</filename>
        (<command>ls</command> y <command>rm</command> son �rdenes UNIX para
        listar o borrar archivos).  Otro car�cter comod�n es ?,
        que corresponde a cualquier s�mbolo individual: por ejemplo:
        <command>rm cap�tulo?.txt</command> borrar� todos los archivos
        <filename>cap�tulo1.txt, cap�tulo2.txt</filename>, pero no el
        <filename>cap�tulo10.txt</filename>
      </para>
      <para>Muchos de los nuevos usuarios de GNOME prefiere utilizar el
          <application>Gestor de Archivos GNOME</application> para
          realizar las operaciones con los archivos, antes que hacerlo
          desde la l�nea de comandos. Los caracteres comod�n tambi�n
          son �tiles para el <application>GMC</application> en los
          di�logos de selecci�n de archivos y de ver filtros.
      </para>
    </sect2>
  </sect1>
  <sect1 id="new-dirs">
    <title>Directorios y rutas</title> 
    <sect2 id="new-dirstruct">
      <title>Estructura de directorios</title>
      <para>
        Ahora, discutiremos el concepto de directorios. Un
        <emphasis>directorio</emphasis> es una colecci�n de archivos.
        Se puede pensar como una ``carpeta'' que contiene muchos
        documentos diferentes. A los directorios se les da nombres,
        por los que pueden ser identificados. M�s a�n, los directorios
        se mantienen en una estructura como de �rbol, es decir,
        el directorio puede contener otros directorios. El directorio de 
        m�s nivel es llamado el "directorio ra�z" y denotado por
        <filename>/</filename>; que contiene los archivos de su sistema.
      </para>
      <sect3 id="new-path">
	<title>Rutas</title>
	<para>
          Una <emphasis>ruta</emphasis> ("path") es realmente como un camino
          a un archivo. Usted puede referirse a un archivo por su ruta,
          que se hace del nombre del documento, precedido por el nombre
          del directorio que contiene ese documento. Este, a su vez, es
          precedido por el nombre del directorio que contiene
          <emphasis>este directorio</emphasis> y as�. Una ruta t�pica
          puede ser as�: <filename>/home/sasha/talk.txt</filename> que
          se refiere al archivo <filename>talk.txt</filename> en el
          directorio <filename>sasha</filename>, el cual a su vez es un
          subdirectorio de <filename>/home</filename>.
	</para>
	<para>
          Como puede ver, el directorio y el nombre del archivo est�n
          separados por una sola barra (/). Por esta raz�n los nombres
          de los archivos no pueden contener en s� mismos el car�cter /.
          Los usuarios de MS-DOS encontrar�n familiar esta convenci�n,
          a pesar de que en el mundo del MS-DOS se utiliza la barra
          invertida (\). El directorio que contiene un subdirectorio dado,
          es conocido como el <emphasis>directorio padre</emphasis>.
          Aqu� el directorio <filename>home</filename> es el padre del
          directorio <filename>sasha</filename>.
	</para>
	<para>
          Cada usuario tiene un directorio personal ("home"), el cual es
          el directorio aparte  que utiliza ese usuario para guardar
          sus archivos. Normalmente, los directorios personales de los
          usuarios est�n contenidos bajo <filename>/home</filename>, y son
          nombrados por el usuario que posee ese directorio, por lo que el
          directorio personal del usuario <systemitem>sasha</systemitem>
          ser�a  <filename>/home/sasha</filename>.
	</para>
      </sect3>
    </sect2>
    <sect2 id="new-relative"> 
      <title>Nombres de directorios relativos</title>
      <para>
        En cualquier momento, las �rdenes que usted introduce son asumidas
        como <emphasis>relativas</emphasis> al directorio actual de trabajo.
        Usted puede pensar que su directorio de trabajo es el directorio en
        el que est� actualmente ``localizado''. Cuando usted se conecta
        por primera vez, su directorio de trabajo es su directorio personal
        &mdash; para el usuario sasha, esto ser�a
        <filename>/casa/sasha</filename>. Cuando quiera referirse a un
        archivo lo puede hacer en relaci�n con su actual directorio
        de trabajo, en lugar de especificar el nombre completo de la ruta
        del archivo.
      </para>
      <para>
        Por ejemplo, si su directorio actual es
        <filename>/home/sasha</filename>, y tiene ah� un archivo llamado
        <filename>talk.txt</filename>, puede referirse a �ste por el nombre
        del archivo: una orden como <command>emacs talk.txt</command>
        ejecutada desde el directorio <filename>/home/sasha</filename>
        es equivalente a <command>emacs /home/sasha/talk.txt</command>
        (<application>emacs</application> es un editor extremadamente
        poderoso para documentos de texto; los nuevos usuarios pueden
        preferir algo m�s simple, tal como <application>gnotepad</application>,
        pero para un usuario avanzado, <application>emacs</application>
        es indispensable).
      </para>
      <para>
        Similarmente, si en <filename>/home/sasha</filename> tiene un
        subdirectorio llamado <filename>textos</filename> y, en ese
        subdirectorio un archivo llamado
        <filename>teoria_campo.txt</filename>, usted puede referirse a
        �ste como <filename>textos/teoria_campo.txt</filename>.
      </para>
      <para> Si usted comienza un nombre de un archivo (como
        <filename>textos/teoria_campo.txt</filename>) con otro car�cter
        que no sea /, usted se est� refiriendo al archivo en t�rminos
        relativos a su actual directorio de trabajo. Esto es conocido como
        una ruta relativa. Por otro lado, si usted comienza el nombre del
        archivo con un /, el sistema interpreta esto como una ruta completa
        &mdash; esto es, una ruta que incluye la ruta completa al archivo,
        comenzando por el directorio ra�z, /.El uso de una ruta completa
        es conocido como una <emphasis>ruta absoluta</emphasis>.
      </para>
    </sect2>
    <sect2 id="new-path-conv">
      <title>Convenciones de ruta</title> 
      <para>
        Aqu� hay algunas convenciones est�ndar que puede utilizar en las rutas:
      </para>
      <para>
	<filename>~/</filename> &mdash; directorio personal del usuario
      </para>
      <para>
	<filename>./</filename>  &mdash; directorio actual de trabajo
      </para>
      <para>
	<filename>../</filename>  &mdash; directorio padre del directorio
        actual
      </para>
      <para>
        Por ejemplo, si el directorio actual del usuario sasha es
        <filename>/home/sasha/papers</filename>, puede referirse al
        archivo <filename>/home/sasha/talk.txt</filename> como
        <filename>~/talk.txt</filename> o como
        <filename>../talk.txt</filename>.
      </para>
    </sect2>
  </sect1>
  <sect1 id="permissions">
    <title>Permisos</title>
    <para>
      Cada archivo en su sistema tiene un <emphasis>due�o</emphasis> &mdash;
      uno de los usuarios (normalmente el que ha creado este archivo), y un
      sistema de permisos que regula el acceso a �ste archivo.
    </para>
    <para>
      Para archivos ordinarios, existen 3 tipos de permisos de acceso:
      leer, escribir y ejecutar ("Read", "Write", "eXecute") ( el �ltimo
      s�lo tiene sentido para archivos ejecutables). Estos permisos pueden
      ser establecidos independientemente para 3 categor�as de usuarios:
      el due�o del archivo, los usuarios en el grupo que posee el archivo
      y todos los dem�s. Las discusiones de grupos de usuarios van m�s all�
      del alcance de este documento; las otras dos categor�as se explican
      por s� mismas. <!--�Qu� grupo de usuarios? No creo que las otras dos
      categor�as se expliquen por s� mismas. Ayudar�a si existiera una
      exposici�n que explicara expl�citamente qui�n pertenece a d�nde.-->
      Por tanto si los permisos en un archivo
      <filename>/home/sasha/talk.txt</filename> est�n establecer para leer
      y escribir por el usuario sasha, quien es el due�o del documento, y ser
      le�do solo por todos los dem�s, s�lo sasha podr� modificar este
      archivo. <!--Qu� tal a�adir algo en par�ntesis aqu�, como (Desde
      que sasha cre� este documento <filename>talk.txt</filename>, sasha
      tiene el m�s amplio rango de derechos de acceso al documento.)....
      o algo por el estilo?-->
    </para>

    <para>
      Todos los nuevos archivos creados llevan algunos permisos est�ndar,
      por lo general leer/escribir para el usuario creador y leer s�lo para
      todos los dem�s. Usted puede ver los permisos utilizando el
      Gestor de Archivos de GNOME, apretando el bot�n derecho del rat�n  en
      el archivo, y escogiendo <guimenuitem>Propiedades</guimenuitem> en el
      men� desplegable, y entonces la pesta�a <guilabel>Permisos</guilabel>.
      Utilizando este di�logo, puede tambi�n cambiar los permisos &mdash;
      s�lo presione en el cuadrado que representa el permiso para modificar
      su estado. Por supuesto, s�lo el due�o del archivo o el administrador
      del sistema puede cambiar los permisos de un archivo. Los usuarios
      avanzados tambi�n pueden cambiar los permisos de los archivos cuando
      se establecen en la creaci�n de los mismos  &mdash; vea las p�ginas
      del manual para su entorno de l�neas de comandos, "shell" (normalmente
      <command>bash</command>, <command>csh</command> o
      <command>tsch</command>) y consulte la orden <command>umask</command>.
    </para>

    <para>
      Un archivo tambi�n puede tener propiedades especiales de permiso
      como UID, GID y bit "sticky". Estos permisos son s�lo para usuarios
      expertos &mdash; no los cambie a menos que usted sepa lo que est�
      haciendo. (Si usted es curioso: estos permisos son t�picamente
      utilizados en archivos ejecutables para permitir al usuario ejecutar
      <emphasis>algunas</emphasis> �rdenes para leer o modificar archivos
      para los cuales el propio usuario no tiene acceso.)
    </para>
    <para> 
      Al igual que los archivos, los directorios tambi�n tienen permisos
      especiales. Otra vez, existen 3 posibles permisos: leer, escribir y
      ejecutar ("Read","Write" y "eXecute"). No obstante, tienen diferente
      significado: el llamado permiso de "leer" para un directorio, significa
      permiso para listar el contenido del directorio o buscar un archivo;
      "escribir" significa permiso para crear y eliminar archivos en el
      directorio, y "ejecutar" significa permiso para acceder a los
      archivos en el directorio.
    </para>
    <para>
      Note que los permisos otorgados a un archivo dependen de los permisos
      del directorio en el cual el documento est� localizado: para ser capaz
      de leer un archivo, el usuario necesita tener el permiso de leer para
      el propio archivo y el permiso "ejecutar" para el directorio que lo
      contiene. Por tanto, si el usuario sasha no quiere que nadie m�s vea
      sus archivos, puede lograr esto eliminando los permisos de ejecuci�n
      de su directorio personal para todos los dem�s usuarios. De esta manera,
      s�lo �l (y, por supuesto, el administrador "root") podr�n leer
      cualquiera de sus archivos, sin importar cuales sean los permisos
      individuales de los archivos.
    </para>
    <para> Una explicaci�n detallada del sistema de permisos puede ser
      encontrada, por ejemplo, en la p�gina "info" para el paquete de
      "Utilidades de Archivos" GNU. Usted puede ver esta p�gina
      de informaci�n, utilizando el explorador de ayuda de GNOME.
      </para>
  </sect1>

  <sect1 id="syslinks">
    <title>Enlaces simb�licos</title>
    <para>
      Adem�s de los archivos regulares, UNIX tiene tambi�n archivos
      especiales llamados <emphasis>enlaces simb�licos</emphasis>
      ("symbolic links" o <emphasis>symlinks</emphasis>, para acortar).
      Estos archivos no contienen datos; en su lugar solo son
      "apuntadores" o "atajos" a otros archivos. Por ejemplo, sasha puede
      tener un symlink llamado <filename>ft.txt</filename> que apunta al
      documento <filename>pruebas/teoriacampo.txt</filename>; de esta manera
      cuando un programa trata de acceder al archivo
      <filename>ft.txt</filename>, el archivo
      <filename>pruebas/teoriacampo.txt</filename> ser� abierto en su lugar.
      Como puede ver por este ejemplo, el symlink y el archivo destino pueden
      tener nombres diferentes y ser localizados en directorios diferentes.
    </para>
    <para> Note que eliminar, mover o renombrar un documento symlink no tiene
      efecto en el archivo destino: si sasha trata de eliminar el
      archivo <filename>ft.txt</filename>, es el symlink lo que se eliminar�,
      y el archivo <filename>pruebas/teoriacampo.txt</filename> seguir�
      inalterado. Tambi�n los permisos del symlink no tienen significado
      alguno, son los permisos del archivo destino los que determinan
      si el usuario tiene acceso a �ste.
    </para>

    <para> Los symlinks tambi�n pueden apuntar a directorios. Por ejemplo,
      en el servidor de FTP de GNOME (<systemitem>ftp.gnome.org</systemitem>),
      existe un archivo <filename>/pub/GNOME/latest</filename>, que en el
      momento en que se escribe este manual, es un symlink al directorio
      <filename>/pub/GNOME/gnome-1.0.53</filename>. Para cuando usted lea
      esto, la �ltima versi�n de GNOME puede haber cambiado y los que
      mantienen GNOME cambiar�an el symlink correspondientemente, que 
      apuntar� a <filename>pub/GNOME/gnome-1.2</filename> o algo similar.
    </para>
  </sect1>
  <sect1 id="new-mount">
    <title>Montando y desmontando dispositivos</title>
    <para>
      Como hemos mencionado anteriormente, los directorios en un entorno
      UNIX est�n organizados en un �rbol, cuyo directorio ra�z es
      <filename>/</filename>. A diferencia de otros sistemas operativos
      como MS-DOS, no hay nombres especiales para los archivos en 
      la unidad de disquete o en el CD-ROM: <emphasis>todos</emphasis>
      los archivos accesibles por su sistema deben aparecen en �l
      �rbol de directorios principal que empieza por <filename>/</filename>.
    </para>
    <para>
      Por tanto, antes de que usted tenga acceso a los archivos en un
      disquete o CD-ROM, usted debe dar a su sistema una orden para
      incorporar los contenidos del disquete en el �rbol directorio
      principal, al cual se le refiere como <emphasis>montaje</emphasis>
      del disquete. T�picamente, los contenidos del CD-ROM aparecen bajo
      el nombre <filename>mnt/cdrom</filename>; los del disquete bajo
      <filename>/mnt/floppy</filename> (�stos son los llamados
      <emphasis>puntos de montaje</emphasis> y son definidos en el
      archivo especial de configuraci�n, <filename>/etc/fstab</filename>).
      El acceso a una unidad, de esta manera, no significa que el sistema
      copiar� todos los archivos del CD al directorio
      <filename>/mnt/cdrom</filename>. En su lugar, esto significa que el
      directorio <filename>/mnt/cdrom</filename> 
      <emphasis>representa</emphasis> al CD-ROM: Cuando un programa trata
      de tener acceso, digamos a un archivo llamado
      <filename>/mnt/cdrom/index.html</filename>, el sistema buscar� el
      archivo <filename>index.html</filename> en el CD-ROM.
    </para>
    <para>
      Por tanto, en pocas palabras: antes de que usted pueda utilizar
      archivos en una unidad, usted debe "montarlo". Similarmente 
      <emphasis>antes de sacar el disco del lector, usted debe
      desmontarlo.</emphasis>
    </para>
    <para>
      Cuando utilice GNOME, usualmente no tiene que preocuparse por montar
      y desmontar: GNOME busca el archivo con la configuraci�n apropiada y
      localiza los iconos para todas las unidades en su escritorio. Al hacer
      doble clic en cualquiera de estos iconos, autom�ticamente se monta la
      unidad correspondiente (si no estaba montado ya) y ejecuta el
      gestor de archivos en el directorio apropiado. Similarmente, si usted
      hace doble clic en el icono unidad y escoge la orden <guimenuitem>Sacar
      disco</guimenuitem> del men� desplegable, GNOME desmonta
      autom�ticamente antes de sacarlo. Usted puede tambi�n montar/desmontar
      una unidad presionando con el bot�n derecho del rat�n en el icono de su
      escritorio y escogiendo <guimenuitem>Montar unidad</guimenuitem> o
      <guimenuitem>Desmontar unidad</guimenuitem> del men� desplegable, o
      utilizando el aplique de montaje de discos.
    </para>
    <para>
      Note que usted no puede desmontar una unidad si est� siendo utilizado
      por alg�n programa; por ejemplo, si usted tiene abierta una ventana
      terminal en el directorio de la unidad que usted est� tratando de
      desmontar. Entonces, recibe el mensaje de error "Controlador ocupado"
      mientras intenta desmontar la unidad, aseg�rese de que ninguna de sus
      aplicaciones abiertas est� teniendo acceso a un archivo o directorio
      en esta unidad.
    </para>
    <para>
      No obstante GNOME no puede impedir que usted saque el disco manualmente
      de la unidad &mdash, en este caso es su responsabilidad el desmontar la
      unidad antes de hacerlo. Para unidades de CD o Zip, el sistema bloquea
      el bot�n de sacado de la unidad mientras la unidad est� montado, para
      los disquetes, esto es t�cnicamente imposible.
    </para>

    <important>
      <title>IMPORTANTE</title>
      <para>
        Si usted saca un disquete utilizando un bot�n de sacado de la unidad
        sin desmontarlo primero, usted puede perder sus datos!
      </para>
    </important>

    <para> Algunos sistemas tienen programas especiales, llamados de
      <emphasis>demonios</emphasis> de automontaje (no necesita saber
      qu� es un demonio), el cual monta autom�ticamente una unidad cuando
      se inserta un disco y desmonta la unidad si �sta no se ha utilizado
      por un per�odo de tiempo espec�fico. En este caso, usted nunca
      deber� preocuparse de montar/desmontar unidades usted mismo.
    </para>
    <para>
      El permitir a los usuarios el montar y desmontar unidades conlleva
      algunos riesgos de seguridad, muchos sistemas se configuran de modo
      que s�lo el usuario administrador "root"  puede montar y desmontar
      una unidad. En este caso, discuta este problema con el administrador
      de su sistema.
    </para>
    <para>
      Si la computadora es su estaci�n de trabajo personal o ordenador
      personal de casa, por lo que no le preocupa la seguridad, usted puede
      dar permiso de montar unidades a usuarios ordinarios. La manera m�s
      f�cil de permitir ello es el uso de la aplicaci�n
      <application><emphasis>linuxconf</emphasis></application> (que s�lo
      puede ser ejecutada por el usuario administrador "root"). S�lo
      seleccione la unidad a la que quiere acceder en la secci�n
      <guilabel>Unidades de acceso local</guilabel> de la pesta�a
      <guilabel>Opciones</guilabel> de la opci�n 
      <guilabel>Montable por usuarios</guilabel>. Su unidad ser� ahora
      montable por los usuarios.
    </para>
    <para>
      Si <application><emphasis>linuxconf</emphasis></application>  no est�
      disponible, usted debe editar el archivo
      <filename>/etc/fstab</filename> para incluir acceso a usuarios. Esto
      se hace a�adiendo el atributo del "usuario" a la unidad. Por ejemplo:
    </para>
    <para>
      Si su archivo fstab contiene una l�nea como �sta: 
    </para>
      <programlisting>
/dev/cdrom /mnt/cdrom iso9660 exec,dev,ro,noauto 0 0
      </programlisting>
      <para>
      a�ada la palabra "usuario" a la cuarta columna:
    </para>
    <programlisting>
/dev/cdrom /mnt/cdrom iso9660 user,exec,dev,ro,noauto 0 0
    </programlisting>
  </sect1>
</appendix>

