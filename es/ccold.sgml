<para>
<guilabel>

Sound Events Tab

</guilabel>

- This tab  allows you to navigate  through the sound  events in GNOME
and change their sounds.

</para>
<para>

To change  a sound associated with  a GNOME event select  the event in
the hierarchical list on the left and press the

<guibutton>

Browse

</guibutton>

button to find a sound file  on your system that you wish to associate
with that event.  Once you have found a sound file you may press the

<guibutton>

Play

</guibutton>

button to  test the sound  and see  if you like  it enough to  hear it
every time the event occurs.

<figure>
<title>

The Sound Properties Capplet

</title>
<screenshot>
<screeninfo>

The Sound Properties Capplet

</screeninfo>
<Graphic Format="png" Fileref="./figs/sound-capplet" srccredit="dcm">
</graphic>
</screenshot>
</figure>
</para>
</sect2>
</sect1>
<sect1 id="gccperiph">
<title>

Peripherals

</title>
<para>
<indexterm id="idx-a81">
<primary>

GNOME Control Center

</primary>
<secondary>

Peripherals

</secondary>
</indexterm>

The  capplets in  this section  of the  Control Center  will  help you
configure  hardware  input   devices  including  keyboard,  and  mouse
properties.

</para>
<sect2 id="gcckey" >
<title>

The Keyboard Properties Capplet

</title>
<para>
<indexterm id="idx-a82">
<primary>

GNOME Control Center

</primary>
<secondary>

Keyboard

</secondary>
</indexterm>

There are  currently two  settings for the  keyboard in  this capplet.
You may change the properties of

<guilabel>

Auto-repeat

</guilabel>

and the

<guilabel>

Keyboard Click

</guilabel>

.

</para>
<para>
<guilabel>

Auto-repeat

</guilabel>

enables you to hold a key down and have it repeat the character at the
rate and delay you set in this capplet.

<guilabel>

Keyboard Click

</guilabel>

enables a small click sound to play at each key press.

<figure>
<title>

The Keyboard Properties Capplet

</title>
<screenshot>
<screeninfo>

The Keyboard Properties Capplet

</screeninfo>
<Graphic Format="png" Fileref="./figs/keyboard-capplet" srccredit="dcm">
</graphic>
</screenshot>
</figure>
</para>
</sect2>
<sect2 id="gccmouse" >
<title>

The Mouse Properties Capplet

</title>
<para>
<indexterm id="idx-a83">
<primary>

GNOME Control Center

</primary>
<secondary>

Mouse

</secondary>
</indexterm>

The Mouse  Properties capplet  allows you to  change between  left and
right handed mouse buttons and to define the

<guilabel>

Accelerations

</guilabel>

and

<guilabel>

Threshold

</guilabel>

properties.

</para>
<para>

The

<guilabel>

Accelerations

</guilabel>

setting  allows you to  change the  speed the  mouse moves  across the
screen in  relation to the  movement of the  mouse on your  mouse pad.
The

<guilabel>

Threshold

</guilabel>

setting allows  you to set  the speed at  which you have to  move your
mouse before it starts the acceleration speed you have defined in the

<guilabel>

Acceleration

</guilabel>

setting.

<figure>
<title>

The Mouse Properties Capplet

</title>
<screenshot>
<screeninfo>

The Mouse Properties Capplet

</screeninfo>
<Graphic Format="png" Fileref="./figs/mouse-capplet" srccredit="dcm">
</graphic>
</screenshot>
</figure>
</para>
</sect2>
</sect1>
<!-- <sect1 id="gccURL" <title>

URL Handlers

</title>
<para>

TO BE DONE...

</para>
</sect1>

-->

<sect1 id="gccsession" <title>

Session Manager

</title>
<para>
<indexterm id="idx-a84">
<primary>

GNOME Control Center

</primary>
<secondary>

Session Manager

</secondary>
</indexterm>

The Session  Manager Capplet allows  you to control the  GNOME Session
Management. This  includes which  programs start up  when you  log in,
whether  to automatically  save your  GNOME requests,  and  whether to
confirm  log out  requests. You  can find  out more  information about
Session Management in

<xref linkend="session">

.

</para>
<ITEMIZEDLIST mark="bullet">
<listitem>
<para>
<guilabel>

Prompt on logout

</guilabel>

- This first option allows you to disable the confirmation dialog when
logging out.

</para>
</listitem>
<listitem>
<para>
<guilabel>

Automatically save changes to session

</guilabel>

- This will  make the Session  Management always save changes  made to
your GNOME session when you log out. If this option is not set you can
still have GNOME save your session changes by checking the appropriate
box in the log out confirmation dialog.

</para>
</listitem>
<listitem>
<para>
<guilabel>

Non-session-managed Startup Programs

</guilabel>

- This allows  you to start non-session  managed applications whenever
you start a GNOME session.

</para>
<note>
<title>

NOTE

</title>
<para>

Programs that are not GNOME  compliant are not session managed so will
not be  restarted without being  listed here. You  do not need  to put
GNOME applications in here, you can simply leave them running and save
the current session when you log out.

</para>
</note>
<para>

If you wish to add a new program to the

<guilabel>

Non-session-managed Startup Programs

</guilabel>

press the

<guibutton>

Add

</guibutton>

button. This  will launch a simple  dialog that allows  you to specify
the  command to  launch  the  application and  what  priority it  will
receive.

</para>
<para>

The priority  for most applications  you wish to  start is 50.  If you
have   an  application  that   needs  to   be  started   before  other
applications, like a window manager,  you should set the priority to a
lower number.

</para>
<important>
<title>

IMPORTANT

</title>
<para>

This option  is for advanced users.  Unless you are  familiar with the
Priority  settings you  should keep  you applications  running  with a
Priority of 50.

</para>
</important>
</listitem>
<listitem>
<para>
<guilabel>

Browse Currently Running Programs

</guilabel>

- This allows you to see  what applications are currently running. You
can shut  down those applications  if you wish and  those applications
will be removed from your GNOME session. The applications in this list
are  mostly   higher  level  applications  and  should   not  be  shut
down. However,  if there is a  part of GNOME  that you do not  wish to
run, like the Panel, this is where  you would shut it down for now and
prevent it from being started when you initiate GNOME in the future.

</para>
<important>
<title>

IMPORTANT

</title>
<para>

This option is  for advanced users only. You should  not use this tool
shut down applications you may wish to use the next time you log in to
GNOME.

</para>
</important>
</listitem>
</itemizedlist>
<figure>
<title>

The Session Manager Capplet

</title>
<screenshot>
<screeninfo>

The Session Manager Capplet

</screeninfo>
<Graphic Format="png" Fileref="./figs/gcc-session" srccredit="dcm">
</graphic>
</screenshot>
</figure>
</sect1>
<sect1 id="gccui">
<title>

User Interface Options

</title>
<para>
<indexterm id="idx-a85">
<primary>

GNOME Control Center

</primary>
<secondary>

User Interface Options

</secondary>
</indexterm>

The  User Interface  Options allows  you to  change the  appearance of
applications  that  are   GNOME-compliant.  You  may  recognize  these
applications as  ones that are  pre-installed with GNOME or  ones that
say they are built with GTK(the GIMP Toolkit).

</para>
<sect2 id="gccappdefaults" <title>

Application Defaults

</title>
<para>
<indexterm id="idx-a86">
<primary>

GNOME Control Center

</primary>
<secondary>

Application Defaults

</secondary>
</indexterm>

The  Application Defaults  capplet allow  you to  change  certain user
interface aspects of your GNOME compliant applications.

</para>
<important>
<title>

IMPORTANT

</title>
<para>

Although this capplet  gives you great control over  the look and feel
of your applications you should  consider these tools for advanced use
only.

</para>
</important>
<ITEMIZEDLIST mark="bullet">
<listitem>
<para>

Can  detach  and  move  menubars   -  By  default  menubars  in  GNOME
applications  may be  dragged  from their  usual  location and  placed
anywhere within the application or desktop.  If you do not wish to use
this feature you may turn it off.

</para>
</listitem>
<listitem>
<para>

Menus  have  relieved borders  -  By  default  menubars have  relieved
borders. If you do not like this look you may turn this feature off.

</para>
</listitem>
<listitem>
<para>

Submenus  can be  torn off  -  This allows  the submenus  to have  the
perforated line which allows you to  "tear" them off an have them as a
small movable window.

</para>
</listitem>
<listitem>
<para>

Menu  items have icons  - Some  menu items  in applications  will have
icons. If  you wish not  to see these  icons in applications  that use
them you may turn off this feature.

</para>
</listitem>
<listitem>
<para>

Statusbar in  interactive when possible  - Some applications  can have
the status bar at the bottom  become separated into its own window. If
you would like to have those applications separate the status bar into
another window you may turn on this option.

</para>
</listitem>
<listitem>
<para>

Statusbar progress  meter on right  - Some applications  have progress
meters in  their statusbars. By  default these progress meters  are on
the right side  of the statusbar. If  you wish them to be  on the left
you may turn off this feature.

</para>
</listitem>
<listitem>
<para>

Can  detach  and  move  toolbars   -  By  default  toolbars  in  GNOME
applications  may be  dragged  from their  usual  location and  placed
anywhere within the application or desktop.  If you do not wish to use
this feature, you may turn it off.

</para>
</listitem>
<listitem>
<para>

Toolbars  have relieved  border -  By default  toolbars  have relieved
borders. If you do not like this look you may turn this feature off.

</para>
</listitem>
<listitem>
<para>

Toolbar buttons have  relieved border - By default  toolbar buttons do
not have  relieved borders in  their natural state. They  do, however,
change when the mouse is over them. If you wish them to be relieved at
all times you may turn on this feature.

</para>
</listitem>
<listitem>
<para>

Toolbars have line separators -  By default toolbar buttons have small
line separators  between them.  If you  so not wish  to have  the line
separators you may turn this feature off.

</para>
</listitem>
<listitem>
<para>

Toolbars have text labels - By default toolbar buttons have images and
text to identify them. If you are familiar with the buttons and do not
need the text you may turn on this feature.

</para>
</listitem>
</itemizedlist>
<figure>
<title>

Applications Defaults Capplet

</title>
<screenshot>
<screeninfo>

Dialog Capplet

</screeninfo>
<graphic Format="png" Fileref="./figs/gccappdef" srccredit="dcm">
</graphic>
</screenshot>
</figure>
</sect2>
<sect2 id="gccdialogs" <title>

Dialogs

</title>
<para>
<indexterm id="idx-a87">
<primary>

GNOME Control Center

</primary>
<secondary>

Dialogs

</secondary>
</indexterm>

The Dialogs Capplet will allow  you to change the default settings for
dialog  boxes in  GNOME compliant  applications.   A dialog  box is  a
window  that is  launched by  an application  to help  perform  a task
needed by  that application.  An example  of a dialog  box is  a Print
dialog which appears when you  press a print button. The dialog allows
you to  set print  options and start  the print process.   The Dialogs
capplet will allow you to change the following options:

</para>
<para>

Dialog  buttons -  Choose to  use  the default  buttons, buttons  more
spread out, put buttons on the edges, put the buttons on the left with
left-justify, and put buttons on the right with right-justify.

</para>
<para>

Dialog buttons have icons - Some dialog buttons (for example "OK") can
have icons  on them.  By default the  applications which  provide this
have the icons turned on. If you wish not to see them you may turn off
this feature.

</para>
<para>

Use  statusbar  instead  of  dialog  when  possible  -  You  may  tell
applications  to  use  the  statusbar  instead  of  a  dialog  if  the
application  will allow  it. This  will  only work  with dialogs  that
provide  information not  one that  require some  interaction  on your
part.

</para>
<para>

Dialog position - This will let you choose how the dialogs appear when
launched. You  can let the window  manager decide for you  (or how you
have  defined it  in  the window  manager  configuration), center  the
dialogs on  the screen, or drop  them where the mouse  pointer is when
they are launched.

</para>
<para>

Dialog hints  - This will  let you change  the behavior of  the dialog
hints  which are the  tooltips that  appear when  you move  your mouse
button over  a button or part of  the dialog.  You may  choose to have
hints handled like other windows, or let the window manager decide how
to display them.

</para>
<para>

Place dialogs over  application window when possible -  You may choose
to place  dialog over the  applications when possible which  will help
you keep  your windows  organized on your  screen If you  are familiar
with other  operating systems  you may wish  to keep this  selected as
this is how most operating systems handle dialogs.

</para>
<important>
<title>

IMPORTANT

</title>
<para>

Although this capplet  gives you great control over  the look and feel
of your applications you should  consider these tools for advanced use
only.

</para>
</important>
<figure>
<title>

Dialog Capplet

</title>
<screenshot>
<screeninfo>

Dialog Capplet

</screeninfo>
<graphic Format="png" Fileref="./figs/gccdialog" srccredit="dcm">
</graphic>
</screenshot>
</figure>
</sect2>
<sect2 id="gccmdi">
<title>

MDI

</title>
<para>
<indexterm id="idx-a88">
<primary>

GNOME Control Center

</primary>
<secondary>

MDI

</secondary>
</indexterm>

The  MDI  capplet  allows  you  to  change  the  MDI  mode  for  GNOME
applications.  MDI  stands for Multiple Document  Interface and refers
to the how more than one document is displayed in GNOME applications.

</para>
<important>
<title>

IMPORTANT

</title>
<para>

Although this capplet  gives you great control over  the look and feel
of your applications you should  consider these tools for advanced use
only.

</para>
</important>
<para>

The default  style in GNOME-compliant applications for  MDI is usually
tabs or "notebooks" If you do not  like the tab look you may change it
here.

</para>
<para>

Default MDI  Mode - In addition  to Notebook, you  will find, Toplevel
and Modal.  Notebook is the  default tab look, Toplevel  displays only
the active document  on the top view until it is  closed and Modal has
only one  toplevel which can contain  any of the documents  at any one
time, however only one can be displayed. If you have ever used

<application>

Emacs

</application>

Modal is very similar to the way

<application>

Emacs

</application>

handles buffers.

</para>
<para>

MDI notebook  tab position - If  you choose to use  the Notebook style
you  may  then decide  where  you  want the  tabs  to  appear in  your
applications. You may have them at  the top, left, right, or bottom of
your  application.  Keep  in  mind  that  these  choices  will  affect
applications that are GNOME compliant.

</para>
<figure>
<title>

MDI Capplet

</title>
<screenshot>
<screeninfo>

MDI Capplet

</screeninfo>
<graphic Format="png" Fileref="./figs/gccmdi" srccredit="dcm">
</graphic>
</screenshot>
</figure>
</sect2>
</sect1>
</chapter>
