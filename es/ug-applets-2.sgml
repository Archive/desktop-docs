 <!-- ############### Monitor Apliques #################### -->
 <sect1 id="monitor-applets">
  <title>Apliques monitor</title>

  <!-- ###############   Monitor de Carga de Bater�a   ############### -->
  <sect2 id="Battery-Charge-Monitor">
   <title>Aplique Monitor de carga de Bateria</title> 

   <para> 
    <application>Monitor de Carga de Bater�a</application> aplique, mostrado en<link
    linkend="Battery-Charge-Monitor-fig1">Figura 1 1</link>, muestra el
    estatus de carga de la bateria de tu portatil. Para aprender como a�adir este
	aplique al <interface>Panel</interface>, 
    haz click con el boton derecho en el<interface>Panel</interface> y elige
    <menuchoice>
      <guimenu>Panel</guimenu>
      <guisubmenu>A�adir al panel</guisubmenu>
      <guisubmenu>Aplique</guisubmenu>
      <guisubmenu>Monitores</guisubmenu>
      <guimenuitem>Monitor de Carga de Bateria</guimenuitem>
    </menuchoice>.
  </para>
  
   <note>
	<title>NOTA</title>
    <para>
     Para que el<application>Monitor de Carga de Bateria
     </application> funcione correctamente, tu computadora debe haber
     sido confugurada para soportar <systemitem>Advanced Power
     Management</systemitem>.
    </para>
   </note>

   <figure id="Battery-Charge-Monitor-fig1">
    <title>Monitor de Carga de Bateria</title>
    <screenshot>
     <screeninfo>Monitor de Carga de Bateria</screeninfo>
     <graphic format="png" fileref="./figs/battery-applet-ac-offline-25-20"
	      srccredit="James Cope">
    </graphic>
    </screenshot>
   </figure>

   <sect3 id="Battery-Charge-Monitor-usage">
    <title>Usage</title>
    <para>
     <application>Monitor de Carga de Bateria</application> se instala en
     tu panel monitoreando el estado de la bateria de tu computadora. Al<mousebutton>clikear</mousebutton>con elboton izquierdo en el aplique tu puedes
     alternar la presentaci�n entre Readout View y Graph View descritas en las secciones 
     <link linkend="Battery-Charge-Monitor-Readout-sec">Readout View</link> 
     y <link linkend="Battery-Charge-Monitor-Graph-sec">Graph View</link> 
     respectivamente.
    </para>

    <sect4 id="Battery-Charge-Monitor-Readout-sec">
     <title>Readout View</title>
     <para>
      Readout View, es la vista por defecto, muestra el estado actual de la bateria
	en forma de texto e icono. En este modo el 
      <application>Monitor de Carga de Bateria</application> muestra la siguiente 
	informaci�n:
     </para>

     <variablelist>
      <varlistentry>
       <term><emphasis>Icono de bateria</emphasis></term>
       <listitem>
	<para>
         La representaci�n de la bateria mediante un icono a la izquierda del 
         aplique, muestra cuan cargada esta la bateria actualmente
         . La parte coloreada de la bateria representa la 
         la cantidad de carga que resta en su batieria, con el tope representa
		que la carga esta al 100%.
	</para>
	
	<para>
	 El color de la bateria cambia cuando la carga baja por debajo del nivel medio
         <guilabel>Low Charge Threshold</guilabel> (see
         <link linkend="Battery-Charge-Monitor-properties-general">Properties &mdash; 
	 General</link>). El color por defecto de la bateriaes verde en estado normal y
         rojo cuando cae por debajo del <guilabel>Low Charge Threshold</guilabel>.
        </para>

	<para>
         Cuando tu computadora est� conectada a la red electrica, una
         peque�a bombilla aparece en la representaci�n de la bateria para
         indicar que la bateria esta conectada a la red electrica y esta
         en estado de carga. Un ejemplo de esto se muestra en la
         <link linkend="Battery-Charge-Monitor-fig2">Figura 2</link>. 
	 tambien se puede configurar la bateria para que cambie de color cuando este
         o no este conectada a la red electrica, mirar
         <link linkend="Battery-Charge-Monitor-properties-readout">
	 Properties &mdash; Readout</link> para instrucciones detalladas sobre esta funcionalidad.
        </para>
      
	<figure id="Battery-Charge-Monitor-fig2">
         <title>Monitor de Carga de Bateria con la red electrica conectada</title>
         <screenshot>
	  <screeninfo>Monitor de Carga de Bateria</screeninfo>
	  <graphic format="png" 
		   fileref="./figs/battery-applet-ac-online-25-20"
		   srccredit="James Cope">
          </graphic>
         </screenshot>
	</figure>

       </listitem>
      </varlistentry>

      <varlistentry>
       <term><emphasis>Porcentaje Restante</emphasis></term>
       <listitem>
	<para>
         El porcentaje restante representado arriba a la derecha del aplique
         representa cuan cargada esta de momento la bateria.
        </para>
       </listitem>
      </varlistentry>

      <varlistentry>
       <term><emphasis>Tiempo Restante</emphasis></term>
       <listitem>
       <para>
        La representaci�n del tiempo restante abajo a la derecha del aplique
        muestra cuanto tiempo restante queda de duraci�n de bateria. El tiempo se muestra
        en horas y minutos.
       </para>
       </listitem>
      </varlistentry>
     </variablelist>
    </sect4>
    
    <sect4 id="Battery-Charge-Monitor-Graph-sec">
     <title>Vista en Gr�fica</title>
     <para>
      La vista en gr�fica muestra el porcentaje el porcentaje de carga restante de
      bateria como un gr�fico cambiante. El eje horizontal representa el tiempo
      y el eje vertical representa el porcentage de carga con el 100%
      de carga estando en lo alto de la gr�fica. Por defecto la grafica es
      verde cuando tu computadora esta conectada a la alimentacion y
      azul cuando no lo esta. La grafica tambien cambia de color cuando la
      carga cae por debajo del <guilabel>Low Charge Threshold</guilabel>
      (see <link linkend="Battery-Charge-Monitor-properties-general">
      Properties &mdash; General</link>) El color por defecto en este caso es rojo.
     </para>

     <figure>
      <title>Monitor de Carga de Bateria en Vista Grafica</title>
       <screenshot>
        <screeninfo>Monitor de Carga de Bateria en Vista Grafica</screeninfo>
        <graphic format="png"
		 fileref="./figs/battery-applet-graph"
		 srccredit="James Cope">
        </graphic>
       </screenshot>
      </figure>
    </sect4>
   </sect3>

   <sect3 id="Battery-Charge-Monitor-right-click">
    <title>Items del Menu desplegable conClick-Derecho</title>
    <para> 
     Ademas de los items del menu estandard, el menu del click-derecho tiene 
     los siguientes items: 
     <itemizedlist> 	
      <listitem>
       <para>
        <guimenuitem>Propiedades...</guimenuitem> &mdash; Este menu item abre
        <interface>Propiedades</interface> dialogo (mirar
        <link linkend="Battery-Charge-Monitor-properties">Properties</link>) 
	el cual permite personalizar la apariencia y el comportamiento de este aplique.
       </para>
      </listitem>
     </itemizedlist>
    </para>
   </sect3>

   <sect3 id="Battery-Charge-Monitor-properties">
    <title>Properties</title>
    <para>
     Puedes configurar<application>El aplique monitor de Carga de Bateria
     </application> presionando click-derecho en el aplique y eligiendo
      <guimenuitem>Propiedades...</guimenuitem> menu.
      Esto abre<interface>Propiedades</interface>
     dialogo, con cuatro grupos de items configurables items dispuestos en las siguientes
      paginas:
     <link
     linkend="Battery-Charge-Monitor-properties-general">General</link>,
     <link
     linkend="Battery-Charge-Monitor-properties-readout">Readout</link>,
     <link
     linkend="Battery-Charge-Monitor-properties-graph">Grafica</link> y
      <link linkend="Battery-Charge-Monitor-properties-messages">Mensajes
      de Carga de Bateria</link>.
    </para>

    <sect4 id="Battery-Charge-Monitor-properties-general">
     <title>Propiedades &mdash; General</title>
    
     <figure id="Battery-Charge-Monitor-properties-fig1">
      <title>Propiedades Dialogo &mdash; General</title>
      <screenshot>
       <screeninfo>Propiedades Dialogo &mdash; General</screeninfo> 
       <graphic format="png" 
		fileref="./figs/battery-applet-properties-general"
		srccredit="James Cope">
        </graphic>
      </screenshot>
     </figure>

     <itemizedlist>
      <listitem>
       <para>
        <guilabel>Sigue Panel de Tama�o</guilabel> &mdash; instruye
	<application>Monitor de Carga de Bateria</application> a redise�ar
	cuando el panel cambia de tama�o. Es chequeado por defecto. Para que 
	 <guilabel>Altura Aplique</guilabel> y
	<guilabel>Ancho Aplique </guilabel> sparametros tomen efecto, esto
		debe estar sin chequeo.
       </para>
      </listitem>

      <listitem>
       <para>
	<guilabel>Altura Aplique &amp; Ancho Aplique</guilabel> &mdash;
	estos dos botones de giro permiten especifiar laqs dimensiones
	de <application>Monitor de Carga de Bateria</application> en pixels exactos
	. Para que estos parametros tomen efecto,
	<guilabel>Siguiente Panel Tama�o</guilabel> debe estar sin chequeo. Loa
		valores por defecto estan puestos a 48.
       </para>

       <note>
		<title>NOTE</title>
	<para>
	 Dejar estos valores muy peque�os (por debajo de 28 pixels)
	 nos pasa que el icono de bateria no aparece debido a la falta de espacio
	 .
        </para>
       </note>
      </listitem>

      <listitem>
       <para>
	<guilabel>Intervalo de Refresco</guilabel> &mdash; specifica
	en segundos cuando <application>El Monitor de Carga de Bateria
	</application> refresca la informacion de carga de bateria
	. El valor por defecto es dos segundos.
       </para>
      </listitem>

      <listitem>
       <para>
	<guilabel>Indicador de Carga Baja</guilabel> &mdash;es un porcentaje
	de la carga de que el 
	<application>Monitor de Carga de Bateria</application> usa para
	opcionalmente mostrar una advertencia, es entonces cuando considera la bateria
	con poca energia remanente. Por ejemplo esta informacion es
	usada para decidir cuanndo debe de cambiar de color del icono de bateria
	. El valor por defecto es 25.
       </para>
      </listitem>

      <listitem>	 
       <para>
	<guilabel>Modo Aplique</guilabel> &mdash; los dos
	<guibutton>Lectura</guibutton> y
	<guibutton>Grafica</guibutton> checkboxes son otra forma de cambiar entre
	 <link linkend="Battery-Charge-Monitor-Readout-sec">
	Vista Lectura</link> and <link linkend="Battery-Charge-Monitor-Graph-sec">
	Vista Grafica</link>.
       </para>
      </listitem>
     </itemizedlist>
    </sect4>

    <sect4 id="Battery-Charge-Monitor-properties-readout">
     <title>Propiedades &mdash; Lectura</title>
     
     <para>
      Los valores aqui solo afectan<application>Monitor de Carga de Bateria
      </application> cuando esta en <link linkend="Battery-Charge-Monitor-Readout-sec">
      Vista Lectura</link>.
     </para>

     <para> 
      Todos los rectangulos coloreados aqui pueden ser
      <mousebutton>izquierdo</mousebutton> clickeados para mostrar la
      rueda de colores GNOME. Esto te permote elegir los colores que prefieras en vez de los de por defecto
      .
     </para>

     <figure id="Battery-Charge-Monitor-properties-fig2">
      <title>Propiedades Dialogo &mdash; Lectura</title>
      <screenshot>
       <screeninfo>Propiedades Dialogo &mdash; Lectura</screeninfo>
       <graphic format="png"
		fileref="./figs/battery-applet-properties-readout" srccredit="James
		Cope"></graphic>
      </screenshot>
     </figure>
	
     <itemizedlist>
      <listitem>
       <para>
	<guilabel>El Color de la Bateria AC-On</guilabel> &mdash; especifica el
	color del icono de bateria en Vista Lectura cuando tu computadora esta
	conectada ala fuente de alimentacion AC . El color por defecto es verde.
       </para>
      </listitem> 

      <listitem>
       <para>
	<guilabel>EL Color de Bateria AC-Off</guilabel> &mdash; especifica
	el colr del icono de bateria en la Vista Lectura cuando tu
	computadora no esta conectada ala fuente de alimentacion AC . El color por
	defecto es el verde
       </para>
      </listitem>


      <listitem>
       <para>
	<guilabel>Clor de Bateria Baja</guilabel> &mdash; especifica el 
	color del icono de bateria en la Vista de Lectura cuando el porcentaje de carga
	cae por debajo del <guilabel>Indicador de Carga Baja
	</guilabel> (see <link
	linkend="Battery-Charge-Monitor-properties-general">Properties 
	&mdash; General</link>). El color por defecto es rojo.
       </para>
      </listitem>
     </itemizedlist>
    </sect4>

    <sect4 id="Battery-Charge-Monitor-properties-graph">
     <title>Propiedades &mdash; Grafica</title>

     <para>
      Los valores aqui solo afectan cuando <application>El Monitor de Carga
      </application> wcuando esta en <link linkend="Battery-Charge-Monitor-Graph-sec">
      Vista Grafica</link>.
     </para>

     <para> Todos los rectangulos coloreados aqui pueden ser
      <mousebutton>derecha</mousebutton> clickeados para mostrar la rueda de colores GNOME
     . Esto te permite elegir los colores que prefieras envez de los de pordefecto
      .
     </para>

     <figure id="Battery-Charge-Monitor-properties-fig3">
      <title>Propiedades Dialogo &mdash; Grafica</title>
      <screenshot>
       <screeninfo>Dialogo Propiedadese &mdash; Grafica</screeninfo>
       <graphic format="png"
		fileref="./figs/battery-applet-properties-graph" srccredit="James
		Cope"></graphic>
      </screenshot>
     </figure>

	
     <itemizedlist>
      <listitem>
       <para>
	<guilabel>Color de Baeria AC-On</guilabel> &mdash;
	especifica el color de la cuando tu computadora esta conectada
	 a la alimentacion AC .El color por defecto es verde
	.
       </para>
      </listitem>
	 
      <listitem>
       <para>
	<guilabel>Color de bateria AC-Off </guilabel> &mdash: especifica
	el color de la grafica cuando tu computadora no esta conectada
	ala alimetacin AC . El color por defecto es azul.
       </para>
      </listitem>

      <listitem>
       <para>
	<guilabel>Color de Grafica de Bateria Baja</guilabel> &mdash; especifica
	el color de la grafica cuando el porcentaje de carga cae por debajo
	 <guilabel>Indicador Carga Baja</guilabel>. El color por defecto es rojo.
       </para>
      </listitem>

      <listitem>
       <para>
	<guilabel>Color de la Marca en Grafica</guilabel> &mdash; especifica el color
	de la linea que hace la division en la grafica,la cual marca el
	25%, 50% y 75% puntos de carga. El color por defecto es gris oscuro.
       </para>
      </listitem>

      <listitem>
       <para>
	<guilabel>Direccion de la Grafica</guilabel> &mdash;
	especifica la direccion en que se mueve la grafica sobre el tiempo. La
	direccion por defecto es <guilabel>Derecha a Izquierda</guilabel>.
       </para>
       </listitem>
     </itemizedlist>
    </sect4>

    <sect4 id="Battery-Charge-Monitor-properties-messages">
     <title>Propiedades &mdash; Mensajes de Carga de Bateria</title>

     <figure id="Battery-Charge-Monitor-properties-fig4">
      <title>Propiedades de Dialogo &mdash; Mensajes Carga de Bateria
        </title>
      <screenshot>
       <screeninfo>Propiedades de Dialogo &mdash;Mensajes Carga de bateria
        </screeninfo>
       <graphic format="png"
		fileref="./figs/battery-applet-properties-messages" srccredit="James
		Cope"></graphic>
      </screenshot>
     </figure>

     <itemizedlist>
      <listitem>
       <para>
	<guilabel>Alerta en la carga de bateria se encuantra por debajo de la marca</guilabel>
	; especifica el porcentaje de carga al cual el
	<application>Monitor de Carga de Bateria</application> muestra un
	cuador de dialogo de alerta que la bateria esta baja. Esta
	alerta es mostrada solo si esta marcado el <guilabel>Permitir Alerta de Bateria Baja
	</guilabel> . El valor por defecto es 5%.
       </para>
      </listitem>

      <listitem>
       <para>
	<guilabel>Permitir Alerta de Bateria Baja</guilabel> &mdash;
	especifica si <application>Monitor de Carga de Bateria</application>
	debe mostrar un cuadro de dialogo de alerta de que la carga de bateria
	ha caido por debajo del porcentaje especificado
	<guilabel>Alerta si el porcentaje de carga cae por debajo del especificado</guilabel>
	. El valor por defecto es chequeado.
       </para>
      </listitem>

      <listitem>
       <para>
	<guilabel>Permitir Notificacion de Carga Completa</guilabel> &mdash;
	especifica si<application>Monitor de Carga de Bateria
	</application> debe mostrar un cuadro de dialogo cuando la
	bateria ha llegado a su 100% de carga. El valor por defecto no es chequeado.
       </para>
      </listitem>
     </itemizedlist>
    </sect4>
<!-- Not needed in for 1.x apliques
    <sect4>
     <title>Informacion A�adida</title>
     <para>
      Para mas informacion<interface>Propiedades</interface>
      dialogo, incluyendo descripciones de el<guibutton>OK</guibutton>,
      <guibutton>Apply</guibutton>, <guibutton>Cerrar</guibutton>, y
      <guibutton>Ayuda</guibutton> botones, ver <xref
      linkend="aplique-properties-dialog">.
     </para>
    </sect4>
-->
   </sect3>
  
   <sect3 id="Battery-Charge-Monitor-bugs">
    <title> Bugs y Limitaciones conocidas</title>
    <para>
     Una contrariedad es si se arranca<application>Monitor de Carga de Bateria
     </application>en un PC que no tiene soporte para Advanced Power
     Management . Esto causa que<guilabel>El cuadro de Alerta de Bateria Baja
     </guilabel> puede ser confuso.     
    </para>
   </sect3>


 </sect2>
  <sect2 id="cpuload-applet">
   <title>El Aplique de Carga deCPU</title> 

   <para> 
     El <application>cpuload_aplique</application> es un peque�o aplique monitor
     el cual reside en tu panel y dice cuanta CPU esta siendo usada. 
   </para>

   <sect3 id="cpuload-usage">
    <title>Usage</title>
    <para>
      No se necesita hacer nada a este aplique. Se encontrara y correra
      en tu panel. Se encuentran varias opciones para alterar su visualizacion.
      </para>

    <figure id="cpuload-applet-fig">
      <title>CPU Load Aplique</title>
      <screenshot>
	<screeninfo>Aplique de Carga de CPU</screeninfo>
	<graphic format="png" fileref="./figs/cpuload-applet" 
	     srccredit="Eric Baudais">
	</graphic>
      </screenshot>
    </figure>

    <para>
      <application>cpuload_aplique</application> divide el uso CPU en cuatro partes:
      </para>
    <variablelist>
     <varlistentry>
      <term>User</term>
      <listitem>
       <para>
         Este mmide el uso de CPU en "userland": Ej, Cosas que no hablan con el  
         kernel. El color por defecto es el amarillo.
       </para>
      </listitem>
     </varlistentry>
 
     <varlistentry>
      <term>System</term>
      <listitem>
       <para>
         Esto mide el uso de CPU que implica el  kernel: Mucho intercambio 
         entre memoria y disco, por ejemplo . El color por defecto es palido
         grey.
       </para>
      </listitem>
     </varlistentry>
 
     <varlistentry>
      <term>Nice</term>
      <listitem>
       <para>
         Esto mide el uso de CPU el cual no es "preciso": corriendo con una prioridad alta.
         EL color por defecto es gris medio. 
       </para>
      </listitem>
     </varlistentry>
 
     <varlistentry>
      <term>Idle</term>
      <listitem>
       <para>
         Medicion de capacidad de CPU desocupada. El color por defeto es negro.
       </para>
      </listitem>
     </varlistentry>
    </variablelist>
   </sect3>
 
   <sect3 id="cpuload-right-click">
    <title>Menus Desplegables con Click-Derecho</title>
    <para> 
     Aprte de los Items estandar en el menu, el menu deplegable con tiene 
     los siguientes items: 
    </para>   

    <variablelist>
     <varlistentry>
      <term>Propiedades por Defecto...</term>
      <listitem>
       <para>
         Esto abre el<guilabel>el cuadro de dialogomultiload_aplique</guilabel>
         <link linkend="cpuload-properties">descrito mas abajo</link> el cual permite 
         alterar las propiedades por defecto de este y otros apliques monitores.
        </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term>Propiedades...</term>
      <listitem>
       <para>
         Esto abre el <guilabel>cuador de dialogo</guilabel>
         <link linkend="cpuload-properties">descrito abajo</link> el cual
         afecta solo <application>a las propiedades cpuload_aplique.</application>
         </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term>Correr gtop...</term>
      <listitem>
       <para>
         Esta opcion corera<command>gtop</command> el cual
         comienza el GNOME System Monitor. <application>gtop</application>
         da en mucho mayor detalle lo que esta pasando en el sistema. 
       </para>
      </listitem>
     </varlistentry>
    </variablelist>
   </sect3>
  
   <sect3 id="cpuload-properties">
    <title>Properties</title>

    <figure id="cpuload-default-fig">
      <title>Dialogo de propiedades por Defecto</title>
      <screenshot>
	<screeninfo>Dialogo de propiedades de carga de CPU </screeninfo>
	<graphic format="png" fileref="./figs/cpuload-applet-default" 
	     srccredit="Eric Baudais">
	</graphic>
      </screenshot>
    </figure>

    <para>
      Al igual que algunos apliques monitores,
      <application>cpuload_aplique</application> tiene dos maneras de definir las preferencias preferences. 
 	Se pueden hacer cambios que afecten a cualquiera de este grupo
      (<application>cpuload_aplique</application>, <application>memload_aplique</application>,
      <application>swapload_aplique</application>, <application>netload_aplique</application>
      and <application>loadavg_aplique</application>)in un cuadro de dialogo largo
      . Esto es util cuando corres uno o varios. Estos son los parametros usados por defecto
      . Se llega a ellos desde 
      <guimenuitem>menu Propiedades por Defecto...</guimenuitem> .
    </para>

    <figure id="cpuload-greyed-fig">
      <title>Dialogo Propiedades Agrisadas</title>
      <screenshot>
	<screeninfo>Apllet carga de CPU Dialogo de popiedades </screeninfo>
	<graphic format="png" fileref="./figs/cpuload-applet-greyed" 
	     srccredit="Eric Baudais">
	</graphic>
      </screenshot>
    </figure>

    <para>
      O se pueden cambiar las propiedades para 
      <application>cpuload_aplique</application>. Esto es util para cuando
      solo se usa<application>cpuload_aplique</application> o quieres probar 
      las nuevas combinaciones. Se llega desde  
      <guimenuitem>Properties</guimenuitem> menu item.
    </para>
    <note>
	  <title>NOTE</title>
     <para>
     Si tu usas el menu<guimenuitem>Propiedades...</guimenuitem> y
      editas las preferencias para este aplique, debes desmarcar
      el <guibutton>Uso de porpiedades por Defecto</guibutton> checkbox antes
     de que puedas alterar las preferencias.
     </para>
    </note>

    <figure id="cpuload-properties-fig">
      <title>Dialogo de Propiedades de CPU </title>
      <screenshot>
	<screeninfo>Dialogo de Propiedades de Carga de CPU </screeninfo>
	<graphic format="png" fileref="./figs/cpuload-applet-properties" 
	     srccredit="Eric Baudais">
	</graphic>
      </screenshot>
    </figure>

    <para>
      Los cambios que s pueden hacer son tres:
    </para>

    <variablelist>
     <varlistentry>
      <term>Colours</term>
      <listitem>
       <para>
         Se pueden alterar los colores usados para las diferentes formas de  
         uso de CPU mostrados al hacer click en las cajas de colores. Esto
         invoca la rueda de colores de GNOME.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term>Speed</term>
      <listitem>
       <para>
         Se puede alterar la velociad conque el grafico trabaja con esto.
         Esta medido en milisegundos. El valor mas alto es 1,000,000,000
         y el menor es 1. El pordefecto es mas sensible y esta a 500.
       </para>
      </listitem>
     </varlistentry>

     <varlistentry>
      <term>Size</term>
      <listitem>
       <para>
         Se puede alterar el tama�o del aplique con estos. Esta medido en pixels
        . El valor por defecto es 40, y el rango va desde 1 pixel
         hasta lo que quieras. En un panel vertical, esto se refiere a la haltura
         . En un panel horizontal , se refiere a la anchura. 
       </para>
      </listitem>
     </varlistentry>
    </variablelist>
   </sect3>
  
   <sect3 id="cpuload-bugs">
    <title>Bugs Conocidos y Limitaciones</title>
    <para>
      Reajustar y por lo tanto alterar la orientacion del panel puede dar como resultado
      problemas intermitentes, aunque se arreglan por si mismos en 
      un corto periodo.
    </para>
   </sect3>

 </sect2>
<sect2 id="cpumemusage-applet">
  <title>Aplet de uso de memoria y de CPU</title> 

  <para> 
    EL<application>cpumemusage_aplique</application> es un peque�o aplique monitor
    el cual se pone en el panely dice sobre el uso de CPU y swap de tu maquina
    .  Para a�adir este
    aplique a <interface>Panel</interface>, 
    haz clic-derecho en<interface>Panel</interface> y elige
    <menuchoice>
      <guimenu>Panel</guimenu>
      <guisubmenu>A�adir al panel</guisubmenu>
      <guisubmenu>Aplique</guisubmenu>
      <guisubmenu>Monitors</guisubmenu>
      <guimenuitem>uso CPU/MEM</guimenuitem>
    </menuchoice>. 
  </para>  
 
   <sect3 id="cpumemusage-usage">
    <title>Uso</title>
    <para>
      No se necesita hacer nada en este aplique. Se instala y corre en tu panel.
    </para>
    <para>
      EL aspplet muestra tres barras de mapas, para la CPU, memoria y espacio de swap. 
      Si tu panel es horizontal,la barra de arriba es CPU, la de enmedio es
      memoria , y la de abajo es swap. Si tu panel es vertical, la de la izquierda
      es CPU, la de enmedio es memoria, y la de la derecha es swap.
    </para>

    <figure id="cpumemusage-applet-fig">
      <title>Aplique de CPU/MEM</title>
     <screenshot>
      <screeninfo>aplique de CPU/MEM</screeninfo>
      <graphic format="png" fileref="./figs/cpumemusage-applet"
      srccredit="Eric Baudais">
      </graphic>
     </screenshot>
    </figure>
  
    <variablelist>
     <varlistentry>
      <term>CPU</term>
      <listitem>
       <para>
         Se usan aqui tres colores. Amarillo es usado para la actividad de CPU 
         la cual no necesita el kernel. Gris es usado para actividad de CPU 
         por programas hablando con el kernel (tipicamente, muchos de I/O como
         cuando se mueven cosas entre memoria y disco).EL negro esta de fondo
         y representa la CPU sin usar. 
       </para>
      </listitem>
     </varlistentry>
     <varlistentry>
      <term>Memoria</term>
      <listitem>
       <para>
         Aqui se usan cuatro colores. Amarillo se usa para la memoria compartida
         (memoria que esta usando mas de un programa). El Girs es usado para
         el uso de buffer (memoria que contiene datos pero aun no ha sido escrito a disco).
         El verde-amarillento se usa para otros tipos de memoria. Verde
         representa la memoria sin usar.
       </para>
      </listitem>
     </varlistentry>
     <varlistentry>
      <term>Swap</term>
      <listitem>
       <para>
         Aqui se usan dos colores. Rojo para el espacio de swap en uso.
         Verde es para el espacio de swap sin usar. El swap es una seccion de disco duro
         donde el kernel pone parte de los programas que no estan en uso en ese momento
         asi puede recuperarlos facilmente cuando los necesite.
       </para>
      </listitem>
     </varlistentry>
    </variablelist>
   </sect3>
<!--  When there are Right-Click Pop-Up Menu Items uncomment this section.
   <sect3 id="cpumemusage-right-click">
    <title>Right-Click Pop-Up Menu Items</title>
    <para> 
      There are no extra menu items in the aplique. 
    </para>
   </sect3>
-->  
   <sect3 id="cpumemusage-bugs">
    <title> Bugs conocidos y Limitaciones</title>
    <para>
      No exsisten bugs conocidos para este aplique.
    </para>
   </sect3>

 </sect2>
<sect2 id="diskusage-applet">
  <title>Disk Usage Aplique</title> 

  <para> <application>Apllet de Uso de Disco</application>, mostrados en<xref
  linkend="diskusage-applet-fig1">, provee un panel monitor para el
  tama�o de disco en uso y disponible en tus discos duros. Para a�adir 
  este aplique al panel, haz click-derecho en el panel y elige
         <menuchoice>
	<guimenu>Panel</guimenu>
	<guisubmenu>A�adir al Panel</guisubmenu>
	<guisubmenu>Aplique</guisubmenu>
	<guisubmenu>Monitores</guisubmenu>
	<guimenuitem>Uso de Disco</guimenuitem>
      </menuchoice>
  </para>
  
 
    <figure id="diskusage-applet-fig1">
     <title>Disk Usage Aplique</title>
     <screenshot>
      <screeninfo>Usos del Apllet de Disco</screeninfo>
      <graphic format="png" fileref="./figs/diskusage-applet" srccredit="John
	       Fleck">
      </graphic>
    </screenshot>
   </figure>

   <!-- ============= Usage  ================================ -->

  <sect3 id="diskusage-applet-usage">
    <title> Uso</title>
    <para><application>Aplique de Uso de Disco</application> provee
    un monitoreo en tiempo real de la cantidad de espacio en tus discos duros.</para>
    <para>Provee tres partes de informacion. La tarta coloreada
    representa graficamente cuanto espacio esta siendo usado y cuanto
    esta disponible. <guilabel>MP:</guilabel> indica el punto de montaje de
    el disco que se esta monitoreando en el momento. <guilabel>av:</guilabel>
    indica la cantidad de espacio disponible en el disco seleccionado, en
    kilobytes. Haciendo click en el aplique cambia el disco que se esta monitoreando.
    Haciendo esto repetidamente hace un ciclo por todos los discos disponibles.</para>
  
  <para>
    Haciendo click con el boton derecho en el aplique arranca un menu que cobntiene los
    siguientes items:
    <itemizedlist>
        <listitem>
	  <para>
	    <guimenuitem>Sistema de Ficheros</guimenuitem> &mdash; te permite
	    cambiar el Sitema de Ficheros <application>Aplique Uso de Disco
	    </application> monitores.</para>
	</listitem>
	<listitem>
	  <para>
	    <guimenuitem>Propiedades</guimenuitem>&mdash; abre las
	    <link linkend="diskusage-applet-prefs">
                <guilabel>Propiedades</guilabel>
            </link>dialogo
	    </para>
	</listitem>

        <listitem>
	  <para>
	    <guimenuitem>Actualizar</guimenuitem> &mdash; este puede ser usado
	    para forzar al aplique a actualizar la lista de Sitemas de ficheros que el aplique usa.
          </para>
	</listitem>
	
	 <listitem>
      <para>
       <guimenuitem>Ayuda</guimenuitem> &mdash;
       muestra este documento.
      </para>
     </listitem>

     <listitem>
      <para>
       <guimenuitem>Acerca de;</guimenuitem> &mdash;
       muestra informacion basic sobre<application>Aplique Uso de Disco
       </application>,incluyendo la vesion de los apliques y el nombre del autor.
      </para>
     </listitem>
	
	

      </itemizedlist>
    </para>
  </sect3>
 <!-- ============= Customization ============================= -->
  <sect3 id="diskusage-applet-prefs">
    <title>Personalizacion</title>
    <para>
      Se puede personalizar <application>Aplique Uso de Disco</application> con
      click-derecho sobre el y eligiendo
      <guimenuitem>Propiedades</guimenuitem>. Esto lanzara el
      <interface>Properties&hellip;</interface>dialogo, que permite que tu cambies
      varios parametros.
    </para>
    <figure id="diskusage-applet-fig2">
     <title>Dialogo de Preferencias</title>
     <screenshot>
      <screeninfo>Dialogo de Preferencias</screeninfo>
      <graphic format="png" fileref="./figs/diskusage-applet-prefs"
	       srccredit="John Fleck">
      </graphic>
    </screenshot>
   </figure>
     <para>
     Las propiedades son:
     <itemizedlist>

      
      <listitem>
       <para>
        <guilabel>Colores</guilabel> &mdash;  Para cambiar los colores
      mostrados en el aplique, haz click en el <guibutton>boton
      coloreado</guibutton> junto al <guilabel>Espacio de Disco Usado</guilabel>,
      <guilabel>Espacio de Disco Libre</guilabel>,
      <guilabel>Color de Texto</guilabel> and
      <guilabel>Color de Fondo</guilabel>. Un dialogo de rueda de colores
      aparecera mostrando opciones de colores para realizar cambios. Cuando acabes
      , haz click en el <guibutton>OK</guibutton> en la ventana de opciones de colores.
       </para>
      </listitem>

      <listitem>
       <para>
        <guilabel>Altura</guilabel> &mdash; <guilabel>Aplique
    Altura</guilabel> y puede ser usado para cambiar la altura (en un 
    panel horizontal) o anchura (en un panel vertical) del aplique.</para>

    <para>Al chequear <guilabel>Automaticamente elige el mejor tama�o del aplique
    </guilabel> cuadro de chequeo, se puede forzar <application>al Aplique de Uso de Disco
    t</application> para que automaticamente elija el tama�o apropiado
    para tu panel.
       </para>
      </listitem>

 <listitem>
       <para>
        <guilabel>Fuentes</guilabel> &mdash; Haciendo click en la barra en esta
        ventana se abre un cuadro de dialogo que permita cambiar la fuente
        mostrada en el apllet.
       </para>
      </listitem>
 <listitem>
       <para>
        <guilabel>Frecuencia de Refresco</guilabel> &mdash; cambia
    la cantidad de tiempo , en segundos, entre refrescos de
    <application>Aplique de uso de Disco</application>.
       </para>
      </listitem>

     </itemizedlist>
    </para>


    <para> 
     </para>

   
   
    <para> Despues de que se hayan hecho todas las selecciones que se deseen, hacer click en
      <guibutton>OK</guibutton> para efectuar los cambios y cerrar las
      <interface>Propiedades</interface>. Para cancelar estos cambios
      y retornar a los valores previos, hacer click en el boton
      <guibutton>Cancelar</guibutton> .
    </para>
  </sect3>
  
 </sect2>
