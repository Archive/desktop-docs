

<!-- #################################### GNOME CONTROL CENTER - CHAPTER ################################## -->
  
  <chapter id="gcc">
    <title>Il Control Center di GNOME</title>  
    <sect1>
      <title>Introduzione</title> 
      <para>  
	<indexterm>  
	  <primary>
	    Control Center di GNOME  
	  </primary>  
	</indexterm>
	<indexterm> 
	  <primary>   Capplets  
	  </primary> 
	  <see>Control Center di GNOME</see> 
	</indexterm> Il Control Center di GNOME vi permette di
	configurare varie parti del sistema usando gli strumenti che
	mette a disposizione, chiamati "capplets". Queste capplets
	possono essere associate al componenti interni delle
	applicazioni di GNOME oppure ad altre applicazioni per cui lo
	sviluppatore a scritto la capplets. 
      </para> 
      <para>
	Il vostro Control Center pu� contenere pi� capplets di quante
	qui documentate, a seconda delle applicazioni installate sul
	vostro sistema. 
      </para> 
      <para>
	Il Control Center in due sezioni principali, il men� con le
	capplet disponibili sul sistema e lo spazio di esecuzione. 
      </para> 
      <para> Per utilizzare il Control Center dovete semplicemente
	selezionare la capplet dal men� sulla sinistra ed eseguire un
	doppio-click su di essa. Una volta fatto questo nello spazio
	di esecuzione verr� mostrata la capplet che avete prescelto,
	permettendovi di configurare la sezione a questa associata. 
      </para> 
      <para> 
	<figure> 
	  <title>Il Control Center di GNOME</title> 
	  <screenshot> 
	    <screeninfo>Il Control Center di GNOME</screeninfo> 
	    <Graphic Format="png"
	      Fileref="./figs/control-center" srccredit="cgabriel">
	    </graphic> 
	  </screenshot> 
	</figure> 
      </para> 
      <para>
	Potete avviare il Control Center in due modi differenti. Per
	lanciare il Control Center senza una particolare capplet
	attiva selezionate la voce <guimenuitem>GNOME Control
	  Center</guimenuitem> dal men�
	<guimenu>Impostazioni</guimenu>.
      </para> 
      <para> Se invece sapete quale capplet usare potete
      avviarla selezionandola nel men� del Control Center oppure
      attraverso una voce del men� <guimenu>Impostazioni</guimenu> nel
      Men� Principale.
      </para>
    </sect1>
    <sect1 id="gccdesktop"
    <title>Capplets per il Desktop</title>
      <sect2 id="gccback">
      <title>Sfondo</title>
	<para>
	  <indexterm>
	    <primary>
	      Control Center di GNOME 
	    </primary> 
	    <secondary>  Desktop
	    </secondary>
	  </indexterm> Le propriet� dello sfondo dell'area di lavoro
	  possono essere impostate qui selezionando un singolo colore
	  oppure una immagine. Se selezionate un colore avete la
	  possibilit� di scegliere fra colore Uniforme e colore
	  Sfumato. Se selezionate Sfumato potete anche scegliere fra
	  sfumatura Verticale oppure Orizzontale e selezionare il
	  secondo colore, l'altro estremo dela sfumatura.
	</para>
	<para>
	  Se decidete invece di avere una immagine come sfondo potete
	  cercare sul disco quale immagine utilizzare. Una volta
	  trovata dovete decidere se dovr� essere Affiancata,
	  Centrata, Ridimensionata mantendo le proporzioni, o
	  semplicemente Ridimensionata. Una volta impostate tutte le
	  opzione premete pure il pulsante
	  <guibutton>Prova</guibutton>, che si trova nella parte bassa
	  del Control Center, per rendere effettive le modifiche
	  apportate. 
	</para> 
	<para>
	  Se desiderate impostare lo sfondo utilizzando altri metodi
	  potet disabilitare questa capplet selezionando
	  <guilabel>Disabilita selezione sfondo</guilabel>. 
	  <figure> 
	    <title>Capplet Propriet� Sfondo</title> 
	    <screenshot> 
	      <screeninfo>Capplet Propriet� Sfondo
	      </screeninfo> 
	      <Graphic Format="png" Fileref="./figs/bg-capplet" srccredit="cgabriel">
	      </graphic> 
	    </screenshot> 
	  </figure> 
	</para> 
      </sect2> 
      <sect2 id="gccscreen"> 
	<title>SalvaSchermo</title> 
	<para> 
	  <indexterm>
	    <primary>  Control Center di GNOME 
	    </primary> 
	    <secondary>
	      SalvaSchermo 
	    </secondary> 
	  </indexterm> In questa capplet si possono cambiare le
	  impostazioni di xscreensaver. Questa capplet contiene la
	  lista dei salva schermo disponibili sul sistema dalla quale
	  potete scegliere e una piccola finestra di anteprima. In
	  basso ci sono gli strumenti per modificare le impostazioni
	  globali per tuttu i salvaschermi. Se quello che avete scelto
	  ha particolare opzioni che possono essere configurate potete
	  cambiare premendo il pulsante
	  <guibutton>Impostazioni</guibutton>, mostrato appena sotto
	  la lista. 
	</para> 
	<para>
	  Impostazioni del salvaschermo - In questa sezione della
	  cappelt potete cambiare le propriet� del tempo di avvio,
	  della password e del Power Management. Potete decidere
	  quanto a lungo si deve attendere prima che venga avviato il
	  salvaschermo digitando il tempo in minuti nella casella di
	  testo <guilabel>Avvia dopo ... minuti</guilabel>. Se
	  desiderate che venga richiesta la password per tornare al
	  desktop di lavoro premete il pulsante <guibutton>Richiedi
	    password</guibutton>. La password per il salvaschermo � la
	  stessa dell'account che avete sul sistema.
	</para> 
	<para>
	  E' disponibile inoltre l'opzione per utilizzare il Power
	  Management - se il vostro monitor supporta questa
	  funzionalit�. Potete qui impostare il tempo di attessa prima
	  che il monitor si spenga digitando il tempo nella casella di
	  testo <guilabel>Spegni il monitor ... minuti dopo che il
	    salvaschermo � partito</guilabel>.
	  <figure> 
	    <title>Capplet SalvaSchermo</title>
	    <screenshot> 
	      <screeninfo>Capplet SalvaSchermo
	      </screeninfo> 
	      <Graphic Format="png" Fileref="./figs/screensave-capplet" srccredit="cgabriel"> 
	      </graphic> 
	    </screenshot> 
	  </figure>
	</para> 
      </sect2> 
      <sect2 id="gcctheme"> 
	<title>Selettore temi</title> 
	<para> 
	  <indexterm> 
	    <primary>  GNOME Control Center 
	    </primary> 
	    <secondary>  Selettore temi
	    </secondary>
	  </indexterm>   Le capplets Desktop contengono il Selettore
	  temi che vi permette di scegliere quale tema GTK da
	  utilizzare.
	</para> 
	<para>
	  I temi per GTK sono pacchetti software che permettono di
	  cambiare l'aspetto del widget set GTK. Il widget set �
	  l'insieme di strumenti che fornisco i pulsanti, le barre di
	  scorrimento, la caselle di spunta etc. per le interfaccie
	  delle applicazioni. Le applicazioni compatibili con GNOME
	  utilizzando GTK quindi la maggior parte delle applicazioni
	  di GNOME cambiaranno aspetto se cambiate il tema di GTK.
	</para> 
	<para>
	  Per cambiare il tema GTK da utilizzare selezionate un tema
	  dalla lista <guilabel>Temi disponibili</guilabel> presente
	  sulla sinistra dello spazio della capplet. Se avete
	  selezionato <guilabel>Anteprima automatica</guilabel> sarete
	  in grado di vedere una anteprima dell'aspetto del tema nella
	  finestra di <guilabel>Anteprima</guilabel> presente in
	  basso. Se il tema � di vostro gradimento premete il pulsante
	  <guibutton>Prova</guibutton> nella zona in basso del Control
	  Center per utilizzare il tema prescelto.
	</para> 
	<para>
	  Ci sono alcuni temi GTK che sono forniti con GNOME quando lo
	  installate. Se desiderate provare altri temi potete
	  controllare fra quelli disponibili su Internet, come ad
	  esempio quelli che si trovano a <ULINK
	    URL="http://gtk.themes.org"
	    type="http">http://gtk.themes.org</ulink>. Una volta
	  trovato un tema di vostro gradimento e scaricato, premete il
	  pulsante <guibutton>Installa nuovo tema...</guibutton>. I
	  file dei temi devono essere nel formato
	  <command>tar.gz</command> oppure <command>.tgz</command>
	  (anche noto come "tarball"). Una volta trovato il file
	  premete <guibutton>OK</guibutton> e il tema verr�
	  automaticamente installato sul sistema. Adesso cercate nella
	  lista dei temi disponibiili quello che avete appena
	  installato.
	</para> 
	  <figure>
	    <title>Capplet Selettori temi</title> 
	    <screenshot>
	      <screeninfo>Capplet Selettore temi
	      </screeninfo>
	      <Graphic Format="png" Fileref="./figs/theme-select" srccredit="cgabriel"> 
	      </graphic> 
	</screenshot> 
      </figure>
      <para>
	  Se desiderate invece cambiare il carattere utilizzato dal
	  tema corrente potete farlo premendo il pulsante
	  <guilabel>Usa font customizzato</guilabel> e quello
	  sottostante, che mostrer� una finestra di dialogo per la
	  selezione dei caratteri che vi permetter� di specificare il
	  carattere, lo stile e la dimensione.
	</para> 
      </sect2> 
      <sect2 id="gccwm">
	<title>Window manager
	</title> 
	<para> 
	  <indexterm> 
	    <primary>GNOME Control Center 
	    </primary> 
	    <secondary>  Window Manager
	    </secondary>
	    <seealso>"Window Manager"</seealso> 
	  </indexterm>  Dato che GNOME non dipende da nessuno window
	  manager particolare questa capplet vi permette di scegliere
	  quale window manager utilizzare. La Capplet Window Manager
	  non determina quali window manager sono disponibili, ma vi
	  permette di farlo, definendo come e dove questi si trovano.
	</para>
	<figure> 
	  <title>Capplet Window Manager</title> 
	  <screenshot> 
	    <screeninfo>Capplet Window Manager
	    </screeninfo> 
	    <Graphic Format="png" Fileref="./figs/wm-main" srccredit="cgabriel"> </graphic>
	  </screenshot> 
	</figure> 
	<para>
	  La capplet Window Manager ha una lista principale di window
	  manager dalla quale potete effettuare la vostra scelta. Il
	  window manager che attivato � marcato con la parola
	  <guilabel>Corrente</guilabel>.
	</para> 
	<para>
	  Se desiderate aggiungere un nuovo window manager alla lista
	  potete premere il pulsante <guibutton>Aggiungi</guibutton>.
	  Questo avvier� la finestra di dialogo <guilabel>Aggiungi un
	    altro window manager</guilabel>.
	</para>
	<figure> 
	  <title>Aggiungi un altro Window Manager</title> 
	  <screenshot>
	    <screeninfo>Aggiungi un altro Window Manager
	    </screeninfo> 
	    <Graphic Format="png" Fileref="./figs/wm-add" srccredit="cgabriel">
	    </graphic> 
	  </screenshot> 
	</figure> 
	<para>
	  Nella finestra <guilabel>Aggiungi un altro window
	    manager</guilabel> potete specificare il nome che
	  desiderate dare al window manager, il comando per avviarlo e
	  il comando per lanciare il programma di configurazione che
	  talvolta � fornito con il window manager.
	</para>
	<para>
	  Se siete coscienti del fatto che il vostro window manager �
	  completamente compatibile con GNOME e pu� essere utilizzato
	  attraverso il gestore di sessione potete selezionare
	  <guilabel>Il window manager supporta il controllo di
	    sessione</guilabel>. Se non siete sicuri consultate la
	  documentazione allegata al window manager stesso.
	</para> 
	<para>
	  Premete <guibutton>OK</guibutton> quando avete inserito
	  tutti i dati richiesti.
	</para>
	<para>
	  Una volta finito di aggiungere il vostro nuovo window
	  manager lo vederete apparire nella lista principale. Se
	  desiderate cambiare qualunque propriet� di questo nuovo
	  oggetto, impostate in precedenza nella finestra di dialogo
	  <guilabel>Aggiungi un altro window manager</guilabel>,
	  potete selezionare il window manager dalla lista con il
	  mouse e premere il pulsante
	  <guibutton>Impostazioni...</guibutton>.
	</para>
	<para>
	  Potete anche cancellare qualunque window manager dalla lista
	  semplicemente selezionandolo e premendo il pulsante
	  <guibutton>Rimuovi</guibutton>
	</para>
	<para>
	  Se siete allora pronti a cambiare il window manager
	  selezionate il nuovo dalla lista e premete il pulsante
	  <guibutton>Prova</guibutton>. Se desiderate avviare il
	  programma di configurazione prima o dopo il cambiamento,
	  assicuratevi che sia selezionato il window manager a cui
	  volete apportare modifiche e premete il pulsante
	  <guibutton>Avvia configurazione di [nome del window
	    manager]</guibutton>.
	</para>
      </sect2>
    </sect1>
    <sect1 id="gccedit">
      <title>Impostazioni Editor</title> 
      <para>
	<indexterm>
	  <primary>
	    Il Control Center di GNOME
	  </primary>
	  <secondary>
	    Impostazioni Editor
	  </secondary>
	</indexterm>  La Capplet di GNOME <guilabel>Impostazioni
	  Editor</guilabel> vi permette di scegliere quale editor
	verr� utilizzato per default mentre usate GNOME. Questo
	permette ad applicazioni come il File Manager di lanciare
	l'editor corretto quando tentate di aprire i file associati
	alla modifica. Sono disponibili tutti gli editor pi� diffusi
	in questa lista. Questa Capplet � molto simile alla Capplet
	Tipi MIME ma viene utilizzata in associazione con determinate
	applicazioni.
      </para>
      <figure>
	<title>Impostazioni Editor</title>
	<screenshot>
	  <screeninfo>
	    Impostazioni Editor
	  </screeninfo>
	  <Graphic Format="png" Fileref="./figs/gccedit"
	  srccredit="cgabriel"> 
	  </graphic>
	</screenshot>
      </figure>
    </sect1>
    <sect1 id="gccmime">
      <title>Tipi MIME</title>
      <para>
	<indexterm>
	  <primary>
	    Il Control Center di GNOME
	  </primary>
	  <secondary>
	    Tipi MIME
	  </secondary>
	</indexterm> La Capplet di GNOME <guilabel>Tipi
	  MIME</guilabel> vi permette di determinare le modalit� in
	cui volete gestire alcuni file, o Tipi MIME. Mime significa
	Multipurpose Internet Mail Extensios ed � stato
	originariamente sviluppato per permettere al sistema di email
	di trasportare svariati tipi di dati. In GNOME potete definire
	svariati tipi Mime per essere gestiti nel modo che preferite.
	Ad esempio, se utilizzate frequentemente file .sgml e
	desidereta utilizzare Emacs come editor predefinito allora
	potete configurare il tipo Mime .sgml per essere sempre
	gestito da Emacs. Questo significa che qualunque programma
	utilizziate quel tipo Mime avvier� sempre Emacs. Questo
	include anche il doppio click effettuato sul file all'interno
	del File Manager di GNOME.
      </para>
      <figure>
	<title>Tipi MIME di GNOME</title> 
	<screenshot>
	  <screeninfo>Tipi MIME di GNOME
	  </screeninfo>
	  <Graphic Format="png"
	  Fileref="./figs/gccmime" srccredit="cgabriel">
	  </graphic>
	</screenshot>
      </figure>
      <para>
	Per aggiungere un nuovo tipo mime premete il pulsante
	<guibutton>Aggiungi...</guibutton>. Questo mostrer� la
	finestra di dialogo <guilabel>Add New Mime Type</guilabel>
	nella quale potete definire il nuovo Tipo Mime.
      </para>
      <para>
	Per modificare un tipo Mime esistente potete selezionarlo con
	il cursore del mouse e premere il pulsante
	<guibutton>Impostazioni...</guibutton>. Questa operazione
	mostrer� la finestra di dialogo <guilabel>Set Actions
	  for...</guilabel>. Potete anche definire il tipo di icone da
	utilizzare per quel tipo Mime, l'azione
	<guilabel>Open</guilabel>, la <guilabel>View</guilabel> e la
	<guilabel>Edit</guilabel>.
      </para>
    </sect1>
    <sect1 id="gccmm">
      <title>Capplets Multimediali</title>
      <sect2 id="gcc-keybell">
	<title>Segnale Acustico</title>
	<para>
	  <indexterm>
	    <primary>
	      Il Control Center di GNOME
	    </primary>
	    <secondary>
	      Multimedia
	    </secondary>
	  </indexterm>
	  <indexterm>
	    <primary>
	      Il Control Center di GNOME
	    </primary>
	    <secondary>
	      Segnale Acustico
	    </secondary>
	  </indexterm> La capplet del Segnale Acustico serve ad
	  impostare il suono prodotto dall'altoparlante del computer
	  quando viene eseguito un errore della tastiera oppure quando
	  un messaggio viene spedito.
	</para>
	<para>
	  Il Volume cambia appunto il volume dello speaker.
	</para>
	<para>
	  La barra delle frequenza serve a cambiare la frequenza della
	  nota che viene riprodotta. L'impostazione standard � 440
	  Hertz, il LA sopra il DO centrale.
	</para>
	<para>
	  La durata, espressa in millisecondi, determina la durata del
	  suono prodotto.
	</para>
	<para>
	  Il pulsante <guibutton>Prova</guibutton> serve per testare
	  le impostazioni correnti del segnale acustico.
	</para>
	<figure>
	  <title>Capplet Segnale Acustico</title>
	  <screenshot>
	    <screeninfo>
	      Capplet Segnale Acustico
	    </screeninfo> <Graphic
	    Format="png" Fileref="./figs/keybell"
	    srccredit="cgabriel">
	    </graphic>
	  </screenshot>
	</figure>
      </sect2>
      <sect2 id="gccsound">
	<title>Audio</title>
	<para>
	  <indexterm>
	    <primary>
	      Il Control Center di GNOME
	    </primary>
	    <secondary>
	      Audio
	    </secondary>
	  </indexterm>  La capplet Audio vi permette di configurare i
	  suoni di sistema eseguiti duranti la sessione di GNOME.
	  Nella parte destra del Control Center trovate due
	  sotto-finestre che potete selezionare:
	  <guilabel>Impostazioni Generali</guilabel> e
	  <guilabel>Eventi Sonori</guilabel>.
	</para>
	<para>
	  <guilabel>Impostazioni Generali</guilabel> - In questa
	  finestra avete la possibilit� di scegliere fra due sole
	  opzioni, Abilitare il suono dentro GNOME e Suoni per eventi.
	  Se selezionate <guilabel>Enable sound server
	    startup</guilabel> vi assicurate che il suond engine di
	  GNOME (chiamato ESD) verr� avviato ad ogni sessione di
	  GNOME. <guilabel>Suoni per eventi</guilabel> permetter� la
	  riproduzione di ogni suono che avete impostato nella
	  finestra <guilabel>Eventi sonori</guilabel> quando
	  determinati eventi accadono.
	</para>
	<para>
	  <guilabel>Eventi sonori</guilabel> - Questa finestra vi
	  permette di navigare attraverso gli eventi sonori di GNOME e
	  cambiare i suoni associati.
	</para>
	<para>
	  Per cambiare un suono associato ad un evento di GNOME
	  selezionato l'evento da moficare nella lista sulla sinistra
	  e premente il pulsante <guibutton>Sfoglia...</guibutton> per
	  cercare il file sonoro all'interno del vostro sistema che
	  desiderate associare a quel particolare evento. Una volta
	  trovato premete il pulsante <guibutton>Suona</guibutton> per
	  provare ad eseguire il suono e vedere se vi piace o meno.
 	  <figure> 
	    <title>Capplet Audio</title>
	    <screenshot> 
	      <screeninfo>
		Capplet Audio
	      </screeninfo> 
	      <Graphic Format="png"
		Fileref="./figs/sound-capplet" srccredit="cgabriel">
	      </graphic> 
	    </screenshot> 
	  </figure> 
	</para> 
      </sect2>
    </sect1> 
    <sect1 id="gccperiph"> 
      <title>Periferiche</title> 
      <para>
	<indexterm> 
	  <primary>  Il Control Center di GNOME
	  </primary>
	  <secondary>  Periferiche
	  </secondary> 
	</indexterm> Le capplets presenti in questa sezione del
	Control Center vi aiuteranno a configurare le periferiche
	hardward, andando a modificare la propriet� della tastiera e
	del mouse.
      </para>
      <sect2 id="gcckey"> 
	<title>Tastiera</title> 
	<para> 
	  <indexterm> 
	    <primary>
	      Il Control Center 
	    </primary> 
	    <secondary>  Tastiera
	    </secondary> 
	  </indexterm>  In questa finestra potete cambiare alcune
	  propriet� della tastiera. La prima opzione che trovate � la
	  <guilabel>Auto-ripetizione</guilabel>, con associate due
	  barre di scorrimento per impostare <guilabel>Velocit� di
	    ripetizione</guilabel> e il <guilabel>Ritardo di
	    ripetizione</guilabel>.
	</para>
	<para>
	  Nella parte successiva potete attivare il <guilabel>Suono
	    della tastiera</guilabel>, attivando il <guilabel>Suono
	    pressione tasto</guilabel>, e modificare il suo volume con
	  la barra di scorrimento denominata <guilabel>Volume
	    suono</guilabel>.
	  <figure> 
	    <title>Capplet Tastiera</title>
	    <screenshot> 
	      <screeninfo>Capplet Tastiera
	      </screeninfo> 
	      <Graphic Format="png"
	      Fileref="./figs/keyboard-capplet" srccredit="cgabriel">
	      </graphic> 
	    </screenshot> 
	  </figure> 
	</para> 
      </sect2> 
      <sect2 id="gccmouse"> 
	<title>Mouse</title>
	<para> 
	  <indexterm> 
	    <primary>  Il Control Center di GNOME 
	    </primary>
	    <secondary>  Mouse 
	    </secondary> 
	  </indexterm> La capplet per le impostazioni del mouse vi
	  permette di rovesciare le posizioni del mouse, se siete
	  mancini oppure destrorsi, e di definire la
	  <guilabel>Accellerazione</guilabel> e la
	  <guilabel>Soglia</guilabel>.
	</para>
	<para>
	  La impostazione per la <guilabel>Accellerazione</guilabel>
	  serve a cambiare la velocit� del mouse quando si sposta
	  attraverso lo schermo in relazione al movimento del mouse
	  sul vostro mouse-pad. La <guilabel>Soglia</guilabel> invece
	  delimita la velocit� minima a cui dovete mouovere il mouse
	  per attivare l'accellerazione cos� come definita nella barra
	  <guilabel>Accellerazione</guilabel>.
	  <figure>
	    <title>Capplet Mouse</title> 
	    <screenshot>
	      <screeninfo>Capplet Mouse
	      </screeninfo>
	      <Graphic Format="png" Fileref="./figs/mouse-capplet"
		srccredit="cgabriel"> 
	      </graphic> 
	    </screenshot> 
	  </figure>
	</para> 
      </sect2> 
    </sect1> 
<!--    <sect1 id="gccsession">
      <title>Programmi di startup</title> 
      <para> 
	<indexterm> 
	  <primary>
	    Il Control Center di GNOME  
	  </primary> 
	  <secondary> 
	    Gestore Sessione 
	  </secondary>
	</indexterm>  
	La capplet GestoreThe Session Manager Capplet allows you to control
      the GNOME Session Management. This includes which programs start
      up, how you save your GNOME configuration, and how you log out.
      You can find out more information about Session Management in
      <xref linkend="session">.  </para> <ITEMIZEDLIST mark="bullet">
      <listitem> <para>     <guilabel>Prompt on logout</guilabel> -
	  This first option allows you to disable the prompt when
	  logging out.  </para> </listitem> <listitem> <para>
	  <guilabel>Automatically save changes to session</guilabel> -
	  This will make the Session Management always save changes
	  made to your GNOME session when you log out. </para>
      </listitem> <listitem> <para> <guilabel>Non-session-managed
	    Startup Programs</guilabel> - This allows you to start
	  non-session managed applications whenever you start a GNOME
	  session.  </para> <note> <title>NOTE</title> <para> Programs
	    that are not GNOME compliant are not session managed so
	    you do not need to put GNOME applications in here, you can
	    simply leave them running and save the current session
	    when you log out. </para> </note> <para>  If you wish to
	  add a new program to the <guilabel>Non-session-managed
	    Startup Programs</guilabel> press the
	  <guibutton>Add</guibutton> button. This will launch a simple
	  dialog that allows you to specify the command to launch the
	  application and what priority it will receive. </para>
	<para> The priority for most applications you wish to start is
	  50. If you have an application that needs to be started
	  before other applications, like a window manager, you should
	  set the priority to a lower number. </para> <important>
	  <title>IMPORTANT</title> <para> This option is for advanced
	    users. Unless you are familiar with the Priority settings
	    you should keep you applications running with a Priority
	    of 50. </para> </important> </listitem> <listitem> <para>
	  <guilabel>Browse Currently Running Programs</guilabel> -
	  This allows you to see what applications are currently
	  running. You may shut down those applications if you wish to
	  and those applications will be removed from your GNOME
	  session. The applications in this list are mostly higher
	  level applications and should not be shut down. However, if
	  there are parts of GNOME that you do not wish to have like
	  the Panel, this is where you would shut it down for now and
	  your next GNOME Session. </para> <important>
	  <title>IMPORTANT</title> <para> This option is for advanced
	    users only. You should not shut down applications you may
	    wish to use the next time you log in to GNOME with this
	    tool. </para> </important> </listitem> </itemizedlist>
    <figure> <title>The Session Manager Capplet</title> <screenshot>
	<screeninfo>The Session Manager Capplet</screeninfo> <Graphic
	  Format="png" Fileref="./figs/gcc-session"
	  srccredit="cgabriel"> </graphic> </screenshot> </figure>
  </sect1>--> 
    <!--  <sect1 id="gccui"> <title>User Interface
  Options</title>  <para> <indexterm> <primary> GNOME Control Center
</primary> <secondary> User Interface Options </secondary>
</indexterm>   The User Interface Options allows  you to  change  the
  appearance  of  applications that  are GNOME-compliant. You may
  recognize these applications as ones that are pre-installed with
  GNOME or  ones that say they are built with GTK(the GIMP Toolkit).
</para> <sect2 id="gccappdefaults" <title>Application Defaults</title>
<para> <indexterm> <primary> GNOME Control Center </primary>
<secondary> Application Defaults </secondary> </indexterm> The
  Application Defaults capplet allow you to change certain user
  interface aspects of your GNOME compliant applications. </para>
<important> <title>IMPORTANT</title> <para> Although this capplet
  gives you great control over the look and feel of your applications
  you should consider these tools for advanced use only. </para>
</important> <ITEMIZEDLIST mark="bullet"> <listitem> <para> Can detach
  and move toolbars - By default toolbars in GNOME applications may be
  dragged from their usual location and placed anywhere within the
  application or desktop. If you do not wish to use this feature you
  may turn it off. </para> </listitem> <listitem> <para> Can detach
  and move men�bars -  By default men�bars in GNOME applications may
  be dragged from their usual location and placed anywhere within the
  application or desktop. If you do not wish to use this feature you
  may turn it off. </para> </listitem> <listitem> <para> Men�bars have
  relieved borders - By default men�bars have relieved borders. If you
  do not like this look you may turn this feature off. </para>
</listitem> <listitem> <para> Toolbars have relieved borders - By
  default toolbars have relieved borders. If you do not like this look
  you may turn this feature off. </para> </listitem> <listitem> <para>
  Toolbar buttons have relieved borders - By default toolbar buttons
  do not have relieved borders in their natural state. They do,
  however, change when the mouse is over them. If you wish them to be
  relieved at all times you may turn on this feature. </para>
</listitem> <listitem> <para> Toolbars have line separators - By
  default toolbar buttons have small line separators between them. If
  you so not wish to have the line separators you may turn this
  feature off. </para> </listitem> <listitem> <para> Toolbars have
  text labels - By default toolbar buttons have images and text to
  identify them. If you are familiar with the buttons and do not need
  the text you may turn on this feature. </para> </listitem>
<listitem> <para> Statusbar in interactive when possible - Some
  applications can have the status bar at the bottom become separated
  into its own window. If you would like to have those applications
  separate the status bar into another window you may turn on this
  option. </para> </listitem> <listitem> <para> Statusbar progress
  meter on right - Some applications have progress meters in their
  statusbars. By default these progress meters are on the right side
  of the statusbar. If you wish them to be on the left you may turn
  off this feature. </para> </listitem> <listitem> <para> Dialog
  buttons have icons - Some dialog buttons (for example "OK") can have
  icons on them. By default the applications which provide this have
  the icons turned on. If you wish not to see them you may turn off
  this feature. </para> </listitem> <listitem> <para> Men� items have
  icons - Some men� items in applications will have icons. If you wish
  not to see these icons in applications that use them you may turn
  off this feature. </para> </listitem> </itemizedlist> <figure>
<title>Applications Defaults Capplet</title> <screenshot>
<screeninfo>Dialog Capplet</screeninfo> <graphic Format="png"
  Fileref="./figs/gccappdef" srccredit="cgabriel"> </graphic>
</screenshot> </figure> </sect2> <sect2 id="gccdialogs"
<title>Dialogs</title> <para> <indexterm> <primary> GNOME Control
  Center </primary> <secondary> Dialogs </secondary> </indexterm> The
  Dialogs Capplet will allow  you to change the  default settings for
  dialog  boxes in GNOME compliant applications.  A dialog box is a
  window that is launched by an application to help perform a task
  needed by that application. An example of a dialog box is a Print
  dialog  which appears when  you press a print  button. The dialog
  allows you  to set  print  options and  start the  print process.
  The  Dialogs  capplet  will  allow you  to  change  the following
  options: </para> <para>Dialog  buttons -  Choose to  use the default
  buttons, buttons  more spread  out, put  buttons on  the edges,  put
  the buttons on the  left with left-justify, and put buttons on the
  right  with right-justify. </para> <para> Default position - This
  will let you choose how the dialogs appear when launched. You can
  let the window manager decide for you (or how you have defined it in
  the window manager configuration), center the dialogs on the screen,
  or drop them where the mouse pointer is when they are launched.
</para> <para> Dialog hints -  This will let you change the behavior
  of the dialog hints which  are the  tooltips that appear  when you
  move your mouse  button over  a button  or part  of the  dialog. You
  may choose to  have hints  handled like other  windows, or  let the
  window manager decide how to display them. </para> <para> You may
  tell applications to use the statusbar instead of a dialog if the
  application will  allow it. This  will only  work with dialogs that
  provide  information  not one  that require  some interaction on
  your part. </para> <para> You may choose to place dialog over the
  applications when possible which will help you keep your windows
  organized on your screen If you are familiar with other operating
  systems  you may wish to keep this selected as this is how most
  operating systems handle dialogs. </para> <important>
<title>IMPORTANT</title> <para> Although this capplet gives you great
  control over the look and feel of your applications you should
  consider these tools for advanced use only. </para> </important>
<figure> <title>Dialog Capplet</title> <screenshot> <screeninfo>Dialog
  Capplet</screeninfo> <graphic Format="png"
  Fileref="./figs/gccdialog" srccredit="cgabriel"> </graphic>
</screenshot> </figure> </sect2> <sect2 id="gccmdi">
<title>MDI</title> <para> <indexterm> <primary> GNOME Control Center
</primary> <secondary> MDI </secondary> </indexterm> The  MDI capplet
  allows  you to  change the  MDI mode  for GNOME applications.  MDI
  stands  for Multiple  Document  Interface and refers to  the how
  more than one document is displayed in GNOME applications.   </para>
<important> <title>IMPORTANT</title> <para> Although this capplet
  gives you great control over the look and feel of your applications
  you should consider these tools for advanced use only. </para>
</important> <para> The default  style in  GNOME-compliant
  applications for  MDI is usually  tabs or "notebooks" If  you do not
  like  the tab look  you may change it here.  </para> <para> In
  addition  to   Notebook,  you   will  find,   Toplevel  and Modal.
  Notebook is the  default tab look, Toplevel displays only the active
  document on the top view until it is closed and Modal has only one
  toplevel which  can contain any of the documents at any one  time,
  however  only one can  be displayed. If  you have ever used
<application>Emacs</application> Modal is very similar to the way
<application>Emacs</application> handles buffers. </para> <para> If
  you choose to use  the Notebook style you  may then decide where you
  want the tabs  to appear in your applications. You may have  them at
  the  top,  left,  right,  or   bottom  of  your application. Keep in
  mind that these choices will  only work in applications that are
  GNOME compliant. </para> <figure> <title>MDI Capplet</title>
<screenshot> <screeninfo>MDI Capplet</screeninfo> <graphic
  Format="png" Fileref="./figs/gccmdi" srccredit="cgabriel">
</graphic> </screenshot> </figure> </sect2> </sect1> -->
  </chapter>